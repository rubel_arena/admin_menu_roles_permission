<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(config('obiroto.adminRoute') . '/dashboards');
})->name('home');



Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);
//Auth::routes();

Route::middleware(['auth'])->namespace('Admin')->group(function () {
    /* ================== News ================== */
    Route::resource('admin/news', 'NewsController');
    Route::get(config('obiroto.adminRoute') . '/news/published/{id}', ['as' => 'admin.news.published', 'uses' => 'NewsController@newsPublished']);
    Route::post(config('obiroto.adminRoute') . '/get-news', ['as' => 'admin.get.news', 'uses' => 'NewsController@newsLoadByAjax']);

    /* ================== Manage Newsletter ================== */
    Route::get(config('obiroto.adminRoute') . '/manage/news-letter', ['uses' => 'NewsLetterController@index']);
    Route::post(config('obiroto.adminRoute') . '/news-letter/load', ['uses' => 'NewsLetterController@loadNewsSchema']);
    Route::post(config('obiroto.adminRoute') . '/news-letter/store', ['as' => 'news_letter.store','uses' => 'NewsLetterController@storeNewsLetter']);

    /* ================== News Comments================== */
    Route::get(config('obiroto.adminRoute') . '/news/{id}/comments', 'CommentsController@index');
    Route::post(config('obiroto.adminRoute') . '/news/{id}/comments/order-update', 'CommentsController@orderUpdate');
    Route::post(config('obiroto.adminRoute') . '/comments/bulk/activation', ['uses'=>'CommentsController@bulk','as'=>'comments.bulk.activation']);

    /* ================== Categories ================== */
    Route::resource(config('obiroto.adminRoute') . '/categories', 'CategoriesController');
    Route::post(config('obiroto.adminRoute') . '/categories/order-update', 'CategoriesController@orderUpdate');

    /* ================== Manage Tag ================== */
    Route::resource(config('obiroto.adminRoute') . '/tags', 'TagController');

    /* ================== Manage Ads ================== */
    //Ads_Types
    Route::resource(config('obiroto.adminRoute') . '/ads_types', 'Ads_TypesController');
    Route::get(config('obiroto.adminRoute') . '/ads_type_dt_ajax', 'Ads_TypesController@dtajax');
    //Ads
    Route::resource(config('obiroto.adminRoute') . '/ads', 'AdsController');
    Route::resource(config('obiroto.adminRoute') . '/get-ads-size', 'AdsController@getSize');
    Route::get(config('obiroto.adminRoute') . '/ad_dt_ajax', 'AdsController@dtajax');

    /* ================== Manage Media ================== */
    //Gallery
    Route::resource(config('obiroto.adminRoute') . '/gallery', 'GalleryController');
    Route::post(config('obiroto.adminRoute') . '/gallery/order-update', 'GalleryController@orderUpdate');


    /* ================== Interviews ================== */
    Route::resource(config('obiroto.adminRoute') . '/interviews', 'InterviewsController');
    Route::get(config('obiroto.adminRoute') . '/interview_dt_ajax', 'InterviewsController@dtajax');

    /* ================== Latest_Jobs ================== */
    Route::resource(config('obiroto.adminRoute') . '/latest_jobs', 'Latest_JobsController');
    Route::get(config('obiroto.adminRoute') . '/latest_job_dt_ajax', 'Latest_JobsController@dtajax');

    /* ================== Common Controller ================== */
    Route::post('upload_image', ['uses' => 'CommonController@uploadImage', 'as' => 'upload']);
    Route::post(config('obiroto.adminRoute').'/load-sub-category', ['uses' => 'CommonController@loadSubCategory', 'as' => 'load-subcategory']);

    /* ==================  Web Settings ================== */
    Route::resource(config('obiroto.adminRoute') . '/settings', 'SiteSettingController');

    /* ==================  Manage Permission ================== */
    Route::resource(config('obiroto.adminRoute') . '/users', 'UsersController');
    Route::resource(config('obiroto.adminRoute') . '/roles', 'RolesController');
    Route::resource(config('obiroto.adminRoute') . '/permissions', 'PermissionController');
    Route::resource(config('obiroto.adminRoute') . '/menues', 'MenuController');
    Route::resource(config('obiroto.adminRoute') . '/home', 'MenuController');

    /* ==================  database migration ================== */
    /*Route::get('migrate-news', 'CommonController@migrateNews');
    Route::get('migrate-category', 'CommonController@migrateCategory');
    Route::get('migrate-image', 'CommonController@migrateImage');
    Route::get('migrate-relations', 'CommonController@migrateRelation');
    Route::get('migrate-image-https', 'CommonController@migrateImageHttps');*/

});

/*Route::get('pages-login', 'VeltrixController@index');
Route::get('pages-login-2', 'VeltrixController@index');
Route::get('pages-register', 'VeltrixController@index');
Route::get('pages-register-2', 'VeltrixController@index');
Route::get('pages-recoverpw', 'VeltrixController@index');
Route::get('pages-recoverpw-2', 'VeltrixController@index');
Route::get('pages-lock-screen', 'VeltrixController@index');
Route::get('pages-lock-screen-2', 'VeltrixController@index');
Route::get('pages-404', 'VeltrixController@index');
Route::get('pages-500', 'VeltrixController@index');
Route::get('pages-maintenance', 'VeltrixController@index');
Route::get('pages-comingsoon', 'VeltrixController@index');*/


Route::resource(config('obiroto.adminRoute') . '/dashboards', 'HomeController');