<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1', 'namespace' => 'API\V1', 'as' => 'api.'], function () {
    Route::get('category/{serial_no}/news','FrontEndController@getNews');
    Route::get('related-news/{category_slug}/{except}','FrontEndController@relatedNews');
    Route::get('categories','FrontEndController@getCategories');
    /*Route::get('interviews', 'FrontEndController@interviews');
    Route::get('interview/{id}', 'FrontEndController@interviewDetail');*/
    Route::get('latest/news', 'FrontEndController@latestNews');
    Route::get('popular/news', 'FrontEndController@popularNews');
    Route::get('archive/news', 'FrontEndController@archiveNews');
    Route::get('latest/jobs', 'FrontEndController@latestJobs');
    Route::get('latest/jobs/{id}', 'FrontEndController@latestJobsDetail');
    Route::get('news/{id}', 'FrontEndController@newsDetails');
    Route::get('news/detail/{slug}', 'FrontEndController@newsDetailsSlug');
    Route::get('tags/{slug}/news', 'FrontEndController@newsDependOnTag');

    Route::get('{category_slug}/news','FrontEndController@categoryNewsList');

    //Manage comments
    Route::post('/comment', 'FrontEndController@storeComment');
    Route::get('comments/{news_id}', 'FrontEndController@comments');

    //Manage Media
    Route::get('/galleries','FrontEndController@galleries');

    //Manage User
    Route::post('register', 'FrontEndController@userRegister');
    Route::get('/user_verification/{token}', 'FrontEndController@user_verification');
    Route::post('login', 'FrontEndController@login');
    Route::post('subscribe', 'FrontEndController@subscribe');
    Route::get('/subscriber_verification/{token}', 'FrontEndController@subscriber_verification');
    Route::post('/social_attempt', 'FrontEndController@socialAttempt');

    //Manage Ads
    Route::get('ads','FrontEndController@getAds');

    //Category wise all news load
    Route::get('category-wise-news-load','FrontEndController@categoryWiseNews');

    //settings
    Route::get('setting','FrontEndController@setting');

    //search
    Route::get('search', 'FrontEndController@search');
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
