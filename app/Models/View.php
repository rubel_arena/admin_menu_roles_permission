<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    protected $fillable = ['counter','viewable_type','viewable_id','read_at','ip_address'];

    public function viewable()
    {
        return $this->morphTo();
    }
}
