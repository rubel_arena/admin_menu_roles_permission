<?php

namespace App\Models;

use App\Repositories\CommonRepo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Menu extends Model
{
    protected $guarded = [];

    public function subMenues()
    {
        return $this->hasMany(Menu::class,'parent_id', 'id');
    }

    public function setMenuNameAttribute($value)
    {
        $this->attributes['menu_name'] = $value;
        $value = Str::lower($value);
        $this->attributes['slug'] = CommonRepo::make_slug($value);
    }
}
