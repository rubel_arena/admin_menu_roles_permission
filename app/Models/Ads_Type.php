<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ads_Type extends Model
{
    use SoftDeletes;
	
	protected $table = 'ads_types';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public function ad()
    {
        return $this->hasOne(Ad::class,'ads_type_id');
	}
}
