<?php

namespace App\Models;

use App\Repositories\CommonRepo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interview extends Model
{
    use SoftDeletes;
	
	protected $table = 'interviews';
	
	protected $hidden = [];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = CommonRepo::make_slug($value);
    }

    public function getCreatedAtAttribute($value)
    {
        return CommonRepo::timestampToBanglaDate($value, 'd F Y');
    }

    public function getListImageAttribute($value)
    {
        return asset($value);
    }

    public function getDetailImageAttribute($value)
    {
        return asset($value);
    }
}
