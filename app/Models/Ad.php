<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use SoftDeletes;
	
	protected $table = 'ads';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public function adType()
    {
        return $this->belongsTo(Ads_Type::class, 'ads_type_id');
	}

    public function getImageAttribute($value)
    {
        return asset($value);
    }
}
