<?php

namespace App\Models;

use App\Repositories\CommonRepo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
	
	protected $table = 'categories';
    protected $hidden = ['pivot'];

    public static function boot()
    {
        parent::boot();
        Static::creating(function ($model) {
            $data = $model::orderBy('serial_no', 'desc')->first();
            $serial_no = !empty($data) ? ($data->serial_no) + 1 : 1;
            $model->serial_no = $serial_no;
        });
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = CommonRepo::make_slug($value);
    }

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public function news()
    {
        return $this->belongsToMany(News::class,'category_news','category_id','news_id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
