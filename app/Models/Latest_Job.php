<?php

namespace App\Models;

use App\Repositories\CommonRepo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Latest_Job extends Model
{
    use SoftDeletes;
	
	protected $table = 'latest_jobs';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public function getCreatedAtAttribute($value)
    {
        return CommonRepo::timestampToBanglaDate($value);
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = CommonRepo::make_slug($value);
    }

    public function getLogoAttribute($value)
    {
        return asset($value);
    }
}
