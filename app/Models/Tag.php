<?php

namespace App\Models;

use App\Repositories\CommonRepo;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];
    protected $hidden = ['pivot'];

    public $timestamps = false;

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = CommonRepo::make_slug($value);
    }

    public function news()
    {
        return $this->morphedByMany(News::class, 'taggable');
    }
}
