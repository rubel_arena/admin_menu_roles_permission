<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $guarded = [];
    public static function boot()
    {
        parent::boot();
        Static::creating(function ($model) {
            $data = $model::orderBy('serial_no', 'desc')->first();
            $serial_no = !empty($data) ? ($data->serial_no) + 1 : 1;
            $model->serial_no = $serial_no;
        });
    }

    public function getListImageAttribute($value)
    {
        return asset($value);
    }
}
