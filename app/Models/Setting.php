<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['brand_name','entrepreneurship_video_link','address','phone','email','logo','favicon','fb_link','twitter_link','youtube_link','pinterest_link'];

    public function getLogoAttribute($value)
    {
        return asset($value);
    }

    public function getFaviconAttribute($value)
    {
        return asset($value);
    }
}
