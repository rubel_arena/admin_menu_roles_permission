<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

	protected $table = 'users';
	//protected $appends = ['profile_image'];
	
	protected $hidden = [];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public function profile()
    {
        return $this->hasOne(User_Profile::class);
	}

    public function donors()
    {
        return $this->hasMany(Donation::class, 'user_id', 'id');
    }

    public function level()
    {
        return $this->belongsTo(User_Level::class, 'user_level','id');
    }
}
