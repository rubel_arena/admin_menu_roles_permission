<?php

namespace App\Models;

use App\Models\Comment;
use App\Models\View;
use App\Repositories\CommonRepo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;
	
	protected $table = 'news';
    protected $hidden = ['pivot'];
    protected $guarded = [];
    //protected $fillable = ['title','slug','description','reporter_id','publisher_id','published_time','is_video','video_link','is_published','ip_address','created_by','updated_by','deleted_by','short_description'];
    protected $dates = ['deleted_at', 'published_time'];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = CommonRepo::make_slug($value);
    }

    public function getPublishedTimeAttribute($value)
    {
        return $value ? CommonRepo::timestampToBanglaDate($value) : '';
    }

    public function user()
    {
        return $this->belongsTo(User::class,'created_by', 'id');
	}

    public function image()
    {
        return $this->hasOne(NewsImage::class,'news_id');
	}

    public function categories()
    {
        return $this->belongsToMany(Category::class,'category_news','news_id','category_id');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function views()
    {
        return $this->morphMany(View::class, 'viewable');
    }

    public function tags()
    {
        return $this->morphToMany( Tag::class, 'taggable' )->withTimestamps();
    }

}
