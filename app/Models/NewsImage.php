<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class NewsImage extends Model
{
    //protected $fillable = ['news_id','caption','highlighted_image','list_image','feature_image','details_image'];
    protected $guarded = [];

   /* public function getImageAttribute($value)
    {
        $contains = Str::contains($value, 'http://www.obiroto.com/wp-content');
        if ($contains) {
            return $value;
        }
        return asset($value);
    }*/

    public function getListImageAttribute($value)
    {
        return asset($value);
    }

    public function getFeatureImageAttribute($value)
    {
        return asset($value);
    }

    public function getHighlightedImageAttribute($value)
    {
        return asset($value);
    }

    public function getDetailsImageAttribute($value)
    {
        return asset($value);
    }
}
