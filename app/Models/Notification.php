<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['recipient_id','sender_id','unread','type','message'];

    public function recipient()
    {
        return $this->belongsTo(User::class,'recipient_id','id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class,'sender_id','id');
    }
}
