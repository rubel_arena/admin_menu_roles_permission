<?php
/**
 * Created by PhpStorm.
 * User: Al-Amin
 * Date: 6/1/2017
 * Time: 11:41 AM
 */

namespace App\Helpers;


use App\Models\Tag;

class Tagging
{
    public static function processTag($tags)
    {
        $taggables = [];
        if (!empty($tags) > 0) {
            foreach ($tags as $tag) {
                if (is_numeric($tag)) {
                    $taggables[] = $tag;
                } else {
                    if ($existsTag=Tag::where('title', $tag)->first()) {
                        $taggables[] = $existsTag->id;
                    } else {
                        $tag = Tag::create(['title' => $tag]);
                        $taggables[] = $tag->id;
                    }
                }
            }
        }
        return $taggables;
    }
}