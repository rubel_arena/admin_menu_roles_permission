<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewsLetter extends Mailable
{
    use Queueable, SerializesModels;
    public $subject;
    public $newsList;
    public $setting;
    public $ad;
    public $interviews;
    public $categoriesNews;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($newsList, $setting, $ad, $interviews, $categoriesNews, $subject)
    {
        $this->newsList = $newsList;
        $this->setting = $setting;
        $this->ad = $ad;
        $this->interviews = $interviews;
        $this->categoriesNews = $categoriesNews;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject_title = isset($this->subject) ?  $this->subject : $this->newsList->first()->title;
        return $this->markdown('emails.newsLetter')->from($this->setting->email, $this->setting->brand_name)
            ->subject($subject_title)
            ->with(['newsList'=> $this->newsList,'setting' => $this->setting, 'ad' => $this->ad, 'interviews' => $this->interviews,'categoriesNews' => $this->categoriesNews]);
    }
}
