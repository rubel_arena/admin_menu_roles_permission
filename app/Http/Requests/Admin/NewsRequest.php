<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd(request()->all());
        return [
            'category_id'   => "required|array|min:1",
            "category_id.*"  => "required|string|distinct|min:1",
            'title' => 'required',
            'description' => 'required',
            'is_published' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif|max:1024'
        ];
    }
}
