<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoriesStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_no' => 'required',
            'nid' => 'required',
            'address' => 'required',
            'user_id' => 'required|integer',
            'showing_position_id' => 'required|integer',
            'amount' => 'required|integer',
            'title' => 'required|unique:stories',
            //'bn_title' => 'required',
            'short_description' => 'required',
            //'bn_short_description' => 'required',
            'video_link' => 'url',
            'is_published' => 'required:in:1,2',
            'story_category_id'   => "required|array|min:1",
            'images'   => "required|array|min:1",
            "images.*"  => "required|image|mimes:jpg,jpeg,png,bmp,tiff|max:4096"
        ];
    }
}
