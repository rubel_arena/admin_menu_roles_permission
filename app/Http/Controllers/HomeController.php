<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Interview;
use App\Models\Latest_Job;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        /*Csv Data Process Start*/
       /* $file = fopen(Storage::get('data.csv'), "r");
        $all_data = array();
        while ( ($data = fgetcsv($file, 1000, ",")) !==FALSE )
        {
            $name = $data[0];
            $city = $data[1];
            $all_data = $name. " ".$city;

            array_push($array, $all_data);
        }
        fclose($file);

        dd($all_data);*/



        /*Csv Data Process end*/

        $totalNews = News::all()->count();
        $totalJobs = Latest_Job::all()->count();
        $totalInterviews = Interview::all()->count();
        $totalCategories = Category::all()->count();
        return view('index', compact('totalNews', 'totalJobs', 'totalInterviews', 'totalCategories'));
    }
}
