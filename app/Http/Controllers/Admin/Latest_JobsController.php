<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Image;

use App\Models\Latest_Job;

class Latest_JobsController extends Controller
{
    /**
     * Display a listing of the Latest_Jobs.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Latest_Job::orderBy('id', 'desc')->get();
        return View('admin.latest_jobs.index', [
            'jobs' => $jobs
        ]);
    }

    /**
     * Show the form for creating a new latest_job.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $published_status = [
            '1' => 'Published',
            '2' => 'Not Published'
        ];
        return view('admin.latest_jobs.form', compact('published_status'));
    }

    /**
     * Store a newly created latest_job in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('logo');
        if ($request->hasFile('logo')) {
            $path = public_path() . '/uploads/latest_jobs/';
            $logo = $request->file('logo');
            $logo_name = 'obiroto-' . uniqid() . '.' . $logo->getClientOriginalExtension();
            $student_path_logo = $path . $logo_name;
            Image::make($logo->getRealPath())->save($student_path_logo);
            $databasePath = str_replace(public_path() . '/', '', $path);
            $data['logo'] = $databasePath . $logo_name;

        }
        Latest_Job::create($data);
        Session::flash('message', Lang::get('messages.Saved successfully'));

        return redirect()->route('latest_jobs.index');
    }

    /**
     * Display the specified latest_job.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified latest_job.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $latest_jobs = Latest_Job::find($id);
        $published_status = [
            '1' => 'Published',
            '2' => 'Not Published'
        ];
        return view('admin.latest_jobs.form')->with(['item' => $latest_jobs, 'published_status' => $published_status]);
    }

    /**
     * Update the specified latest_job in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $latest_jobs = Latest_Job::find($id);
        $data = $request->except('logo');
        if ($request->hasFile('logo')) {
            $path = public_path() . '/uploads/latest_jobs/';
            $logo = $request->file('logo');
            $logo_name = 'obiroto-' . uniqid() . '.' . $logo->getClientOriginalExtension();
            $logo_path = $path . $logo_name;

            $project_path_logo_old = public_path() . $latest_jobs->logo;
            if (file_exists($project_path_logo_old)) {
                //first unlink the logo
                @unlink($project_path_logo_old);
            }
            Image::make($logo->getRealPath())->save($logo_path);
            $databasePath = str_replace(public_path() . '/', '', $path);
            $data['logo'] = $databasePath . $logo_name;
        }

        $latest_jobs->update($data);
        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('latest_jobs.index');

    }

    /**
     * Remove the specified latest_job from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $latest_jobs = Latest_Job::find($id);
        $latest_jobs->forceDelete();
        Session::flash('message', Lang::get('messages.Deleted successfully'));
        return redirect()->route('latest_jobs.index');
    }

}
