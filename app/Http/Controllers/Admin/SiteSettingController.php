<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SiteSettingRequest;
use App\Models\Setting;
use App\Repositories\ImageUploadRepo;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class SiteSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = Setting::first();
        return view('admin.settings.index', compact('item'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(SiteSettingRequest $request, $id)
    {
        $data = $request->except(['logo','favicon']);
        $site_setting = Setting::find($id);
        $path = public_path() . '/' . '/uploads/settings/';


        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            $image_name = 'obiroto-'.uniqid().'.'.$logo->getClientOriginalExtension();
            $image_path=$path . $image_name;

            $project_path_image_old= public_path() .$site_setting->logo;
            if (file_exists($project_path_image_old)) {
                //first unlink the image
                @unlink($project_path_image_old);
            }
            Image::make($logo->getRealPath())->save($image_path);
            $databasePath = str_replace(public_path().'/', '',$path);
            $data['logo']=$databasePath.$image_name;
        }

        //Logo
        if ($request->hasFile('favicon')) {
            $data['favicon'] = ImageUploadRepo::uploadImage($path, $request->favicon, '16-16', $site_setting->favicon);
        }

        //update site settings
        $site_setting->update($data);
        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('settings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
