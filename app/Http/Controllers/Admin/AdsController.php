<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ads_Type;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use App\Models\Ad;
use Image;

class AdsController extends Controller
{
    /**
     * Display a listing of the Ads.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::with('adType')->orderBy('id', 'desc')->get();

        return View('admin.ads.index', [
            'ads' => $ads
        ]);
    }

    /**
     * Show the form for creating a new ad.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $adsTypes = Ads_Type::has('ad', '<', 1)->get()->pluck('title', 'id')->prepend('Please Select', '');
        return view('admin.ads.form', compact('adsTypes'));
    }

    /**
     * Store a newly created ad in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Admin\AdsStoreRequest $request)
    {
        $data = $request->except('image');

        if ($request->hasFile('image')) {
            $path = public_path() . '/uploads/ads/';
            $image = $request->file('image');
            $image_name = 'obiroto-' . uniqid() . '.' . $image->getClientOriginalExtension();
            $student_path_image = $path . $image_name;
            Image::make($image->getRealPath())->save($student_path_image);
            $databasePath = str_replace(public_path() . '/', '', $path);
            $data['image'] = $databasePath . $image_name;

        }
        Ad::create($data);
        Session::flash('message', Lang::get('messages.Saved successfully'));
        return redirect()->route('ads.index');
    }

    /**
     * Display the specified ad.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified ad.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Ad::find($id);
        $adsTypes = Ads_Type::get()->pluck('title', 'id')->prepend('Please Select', '');
        return view('admin.ads.form', compact('item', 'adsTypes'));
    }

    /**
     * Update the specified ad in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ad = Ad::find($id);
        $data = $request->except('image');
        //Image 1
        if ($request->hasFile('image')) {
            $path = public_path() . '/uploads/ads/';
            $image = $request->file('image');
            $image_name = 'obiroto-' . uniqid() . '.' . $image->getClientOriginalExtension();
            $image_path = $path . $image_name;

            $project_path_image_old = public_path() . $ad->image;
            if (file_exists($project_path_image_old)) {
                //first unlink the image
                @unlink($project_path_image_old);
            }
            Image::make($image->getRealPath())->save($image_path);
            $databasePath = str_replace(public_path() . '/', '', $path);
            $data['image'] = $databasePath . $image_name;
        }
        $ad->update($data);
        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('ads.index');
    }

    /**
     * Remove the specified ad from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Ad::find($id);
        $ad->forceDelete();
        Session::flash('message', Lang::get('messages.Deleted successfully'));
        return redirect()->route('ads.index');
    }

    public function getSize(Request $request)
    {
        if ($request->ajax()) {
            $type = Ads_Type::where('id', $request->id)->first();
            if (!empty($type)) {
                return response()->json(['data' => $type->image_size]);
            } else {
                return response()->json(['data' => '0']);
            }
        }
    }
}
