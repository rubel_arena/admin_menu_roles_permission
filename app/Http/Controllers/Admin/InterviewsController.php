<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ImageUploadRepo;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

use App\Models\Interview;

class InterviewsController extends Controller
{
    /**
     * Display a listing of the Interviews.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = Interview::orderBy('id', 'desc')->get();
        return View('admin.interviews.index', [
            'interviews' => $interviews
        ]);
    }

    /**
     * Show the form for creating a new interview.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $published_status = [
            '1' => 'Published',
            '2' => 'Not Published'
        ];
        return view('admin.interviews.form', compact('published_status'));

    }

    /**
     * Store a newly created interview in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\Admin\InterviewRequest $request)
    {
        $data = $request->except('image','is_published');
        if ($request->hasFile('image')) {
            $image = $request->image;
            $path = public_path() . "/uploads/interviews/";
            $data['list_image'] = ImageUploadRepo::uploadImage($path, $image, '120-110');
            $data['detail_image'] = ImageUploadRepo::uploadImage($path, $image, '830-400');

        }
        $data['status'] = $request->is_published;
        Interview::create($data);
        Session::flash('message', Lang::get('messages.Saved successfully'));
        return redirect()->route('interviews.index');
    }

    /**
     * Display the specified interview.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified interview.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $interviews = Interview::find($id);
        $published_status = [
            '1' => 'Published',
            '2' => 'Not Published'
        ];
        return view('admin.interviews.form')->with(['item' => $interviews, 'published_status' => $published_status]);
    }

    /**
     * Update the specified interview in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $interviews = Interview::find($id);
        $data = $request->except('image','is_published');
        if ($request->hasFile('image')) {
            $image = $request->image;
            $path = public_path() . "/uploads/interviews/";
            //$imageData['news_id'] = $news->id;
            $data['list_image'] = ImageUploadRepo::uploadImage($path, $image, '120-110', $interviews->list_image);
            $data['detail_image'] = ImageUploadRepo::uploadImage($path, $image, '830-400', $interviews->detail_image);

        }
        $data['status'] = $request->is_published;
        $interviews->update($data);
        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('interviews.index');
    }

    /**
     * Remove the specified interview from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $interviews = Interview::find($id);
        $interviews->forceDelete();
        Session::flash('message', Lang::get('messages.Deleted successfully'));
        return redirect()->route('interviews.index');
    }

}
