<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comment;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use URL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CommentsController extends Controller
{
    public function index($id)
    {
        $published_status = [
            '1' => 'Published',
            '2' => 'Not Published',
            '3' => 'Canceled',
        ];
        $news = News::with(['comments' => function ($q) {
            $q->orderBy('id', 'desc');
        }])->find($id);
        return view('admin.news.comments', compact('news', 'published_status'));
    }

    public function store(Request $request)
    {
        if (Auth::check()) {
            $story = Story::find($request->story_id);
            $data = $request->all();
            $story->comments()->create($data);
            Session::flash('message', Lang::get('messages.Thank Your For Your Comment.This Comment Show in Website After Approval'));
            return redirect(url(URL::previous()));
        } else {
            return redirect('signin');
        }
    }

    public function orderUpdate($story_id, Request $request)
    {
        $values = Comment::where(['commentable_id' => $story_id, 'commentable_type' => 'stories'])->orderBy('serial_no', 'asc')->get();
        foreach ($values as $item) {
            $item->timestamps = false;
            $id = $item->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $item->update(['serial_no' => $order['position']]);
                }
            }
        }
        return response()->json('Update Successfully.', 200);
    }

    public function bulk(Request $request)
    {
        Comment::whereIn('id', $request->comments)->update([
            'is_published' => $request->is_published
        ]);
        Session::flash('message', Lang::get('messages.Saved successfully'));
        return redirect(url(URL::previous()));
    }
}
