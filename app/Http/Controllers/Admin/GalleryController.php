<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Repositories\ImageUploadRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::orderBy('serial_no', 'asc')->get();
        return View('admin.galleries.index', [
            'galleries' => $galleries
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.galleries.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('image');
        if ($request->hasFile('image')) {
            $image = $request->image;
            $path = public_path() . "/uploads/gallery/";
            $data['list_image'] = ImageUploadRepo::uploadImage($path, $image, '270-180');
            $data['preview_image'] = ImageUploadRepo::uploadImage($path, $image, '400-270');

        }
        Gallery::create($data);
        Session::flash('message', Lang::get('messages.Saved successfully'));
        return redirect()->route('gallery.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        return view('admin.galleries.form')->with(['item' => $gallery]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {
        $data = $request->except('image');
        if ($request->hasFile('image')) {
            $image = $request->image;
            $path = public_path() . "/uploads/gallery/";
            $data['list_image'] = ImageUploadRepo::uploadImage($path, $image, '120-110', $gallery->list_image);
            $data['preview_image'] = ImageUploadRepo::uploadImage($path, $image, '830-400', $gallery->detail_image);

        }
        $gallery->update($data);
        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('gallery.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        $gallery->forceDelete();
        Session::flash('message', Lang::get('messages.Deleted successfully'));
        return redirect()->route('gallery.index');
    }

    public function orderUpdate(Request $request)
    {
        $values = Gallery::orderBy('serial_no', 'asc')->get();
        foreach ($values as $item) {
            $item->timestamps = false;
            $id = $item->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $item->update(['serial_no' => $order['position']]);
                }
            }
        }
        return response()->json('Update Successfully.', 200);
    }
}
