<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsRequest;
use App\Models\Category;
use App\Repositories\ImageUploadRepo;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Models\News;
use Illuminate\Support\Facades\Session;

class NewsController extends Controller
{
    /**
     * Display a listing of the News.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::with('categories', 'user')->withCount('comments')->orderBy('id', 'desc')->get();
        return View('admin.news.index', [
            'news' => $news
        ]);
    }

    /**
     * Show the form for creating a new news.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where(['is_published' => 1, 'category_id' => 0])->orderBy('serial_no')->get()->pluck('title', 'id');
        $published_status = [
            '1' => 'Published',
            '2' => 'Not Published'
        ];
        return view('admin.news.form', compact('categories', 'published_status'));
    }

    /**
     * Store a newly created news in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request)
    {
        $userId = auth()->id();
        $newsData = $request->except('category_id', 'image');
        $newsData['reporter_id'] = $userId;
        $newsData['publisher_id'] = ($request->is_published == 1) ? $userId : null;
        $newsData['published_time'] = ($request->is_published == 1) ? Carbon::now() : null;
        $newsData['ip_address'] = CommonHelper::getRealIpAddr();
        $newsData['created_by'] = $userId;
     /*   $newsData['updated_by'] = NULL;
        $newsData['deleted_by'] = NULL;*/
       // dd($newsData);
        $news = News::create($newsData);

        $news->categories()->sync($request->category_id);

        if ($request->hasFile('image')) {
            $image = $request->image;
            $path = public_path() . "/uploads/news/$news->id" . '/';
            $imageData['image'] = ImageUploadRepo::uploadImage($path, $image, '650-550');
            $news->image()->create($imageData);
        }

        Session::flash('message', Lang::get('messages.Saved successfully'));
        return redirect()->route('news.index');
    }

    /**
     * Display the specified news.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        imagewebp($im, 'php.webp');
        return 'hello';
    }

    /**
     * Show the form for editing the specified news.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $categories = Category::where('is_published', 1)->orderBy('serial_no')->get()->pluck('title', 'id');
        $category_ids = collect(DB::table('category_news')->where('news_id', $news->id)->get())->pluck(['category_id'])->toArray();
        $published_status = [
            '1' => 'Published',
            '2' => 'Not Published'
        ];
        return view('admin.news.form')->with(['item' => $news, 'categories' => $categories, 'published_status' => $published_status, 'category_ids' => $category_ids]);
    }

    /**
     * Update the specified news in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $userId = auth()->id();
        $newsData = $request->except('category_id', 'image');
        $newsData['reporter_id'] = $userId;
        $newsData['publisher_id'] = ($news->is_published !== 1 && $request->is_published == 1) ? $userId : $news->publisher_id;
        $newsData['published_time'] = ($news->is_published !== 1 && $request->is_published == 1) ? Carbon::now() : null;
        $newsData['ip_address'] = CommonHelper::getRealIpAddr();
        $newsData['updated_by'] = $userId;
        $news->update($newsData);
        $news->categories()->sync($request->category_id);

        if ($request->hasFile('image')) {
            $existiongImage = $news->image;
            $image = $request->image;
            $path = public_path() . "/uploads/news/$news->id" . '/';
            //$imageData['news_id'] = $news->id;
            $imageData['image'] = ImageUploadRepo::uploadImage($path, $image, '650-550', $existiongImage->image);
            $news->image()->update($imageData);

        }

        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('news.index');
    }

    /**
     * Remove the specified news from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->categories()->detach();
        $news->delete();
        Session::flash('message', Lang::get('messages.Deleted successfully'));
        return redirect()->route('news.index');
    }

    /**
     * Datatable Ajax fetch
     *
     * @return
     */

    public function newsPublished($id)
    {
        $userId = auth()->id();
        $news = News::where('id', $id)->first();
        $news->is_published = 1;
        $news->updated_by = $userId;
        $news->publisher_id = $userId;
        $news->published_time = Carbon::now();
        $news->save();
        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('news.index');
    }
}
