<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Collective\Html\FormFacade as Form;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

use App\Models\Ads_Type;

class Ads_TypesController extends Controller
{

    /**
     * Display a listing of the Ads_Types.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ads_Type::orderBy('id', 'desc')->get();

        return View('admin.ads_types.index', [
            'ads' => $ads
        ]);
    }

    /**
     * Show the form for creating a new ads_type.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ads_types.form');
    }

    /**
     * Store a newly created ads_type in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['no_of_image'] = 1;
        Ads_Type::create($data);
        Session::flash('message', Lang::get('messages.Saved successfully'));
        return redirect()->route('ads_types.index');
    }

    /**
     * Display the specified ads_type.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified ads_type.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Ads_Type::where('id', $id)->first();
        return view('admin.ads_types.form', compact('item'));
    }

    /**
     * Update the specified ads_type in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $adsType = Ads_Type::where('id', $id)->first();
        $data = $request->all();
        $adsType->update($data);
        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('ads_types.index');
    }

    /**
     * Remove the specified ads_type from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ads_Type::find($id)->delete();
        Session::flash('message', Lang::get('messages.Deleted successfully'));
        return redirect()->route('ads_types.index');
    }

}
