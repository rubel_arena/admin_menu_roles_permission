<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\NewsLetterHistory;
use Illuminate\Http\Request;

class NewsLetterHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NewsLetterHistory  $newsLetterHistory
     * @return \Illuminate\Http\Response
     */
    public function show(NewsLetterHistory $newsLetterHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NewsLetterHistory  $newsLetterHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(NewsLetterHistory $newsLetterHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NewsLetterHistory  $newsLetterHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewsLetterHistory $newsLetterHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NewsLetterHistory  $newsLetterHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewsLetterHistory $newsLetterHistory)
    {
        //
    }
}
