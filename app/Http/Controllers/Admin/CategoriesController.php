<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoriesRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

use App\Models\Category;

class CategoriesController extends Controller
{

    /**
     * Display a listing of the Categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('parent')->orderBy('order_no', 'asc')->get();
        return View('admin.categories.index', [
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where(['is_published' => 1, 'category_id' => 0])->orderBy('serial_no')->get()->pluck('title', 'id')->prepend('Please select', '');
        $published_status = [
            '1' => 'Published',
            '2' => 'Not Published'
        ];

        $is_show_menu = [
            '1' => 'Yes',
            '2' => 'No'
        ];

        return view('admin.categories.form', compact('categories', 'published_status', 'is_show_menu'));
    }

    /**
     * Store a newly created category in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesRequest $request)
    {
        $data = $request->except('category_id');
        $data['category_id'] = !empty($request->category_id) ? $request->category_id : 0;
        Category::create($data);
        Session::flash('message', Lang::get('messages.Saved successfully'));
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified category.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::where('id', $id)->first();
        $categoriesList = Category::where(['is_published' => 1, 'category_id' => 0])->get()->pluck('title', 'id')->prepend('Please select', '');
        $published_status = [
            '1' => 'Published',
            '2' => 'Not Published'
        ];
        $is_show_menu = [
            '1' => 'Yes',
            '2' => 'No'
        ];

        return view('admin.categories.form')
            ->with(['item' => $category, 'categories' => $categoriesList, 'published_status' => $published_status, 'is_show_menu' => $is_show_menu]);
    }

    /**
     * Update the specified category in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::where('id', $id)->first();
        $data = $request->except('category_id');
        $data['category_id'] = !empty($request->category_id) ? $request->category_id : 0;
        $category->update($data);
        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified category from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::where('id', $id)->first();
        $category->forceDelete();
        Session::flash('message', Lang::get('messages.Deleted successfully'));
        return redirect()->route('categories.index');
    }

    /**
     * Datatable Ajax fetch
     *
     * @return
     */

    public function orderUpdate(Request $request)
    {
        $values = Category::orderBy('order_no', 'asc')->get();
        foreach ($values as $item) {
            $item->timestamps = false;
            $id = $item->id;

            foreach ($request->order as $order) {
                if ($order['id'] == $id) {
                    $item->update(['order_no' => $order['position']]);
                }
            }
        }
        return response()->json('Update Successfully.', 200);
    }
}
