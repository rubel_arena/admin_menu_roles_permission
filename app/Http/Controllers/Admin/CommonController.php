<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Category;
use App\Models\News;
use App\Models\NewsImage;
use App\Repositories\ImageUploadRepo;
use Illuminate\Http\Request;
use Collective\Html\FormFacade as Form;
use App\Helpers\CommonHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\Input;

class CommonController extends Controller
{

    public function migrateCategory()
    {
        $categories = DB::select("SELECT 
            ca.term_id id, ca.name title, ca.slug, car.parent category_id
        FROM
            wp_eqbw_terms ca
                INNER JOIN
            wp_eqbw_term_taxonomy car ON ca.term_id = car.term_id
        WHERE
            car.taxonomy = 'category'
        ORDER BY ca.term_id");

        foreach ($categories as $category) {
            $serial_no = 1;
            $data['id'] = $category->id;
            $data['title'] = $category->title;
            $data['slug'] = $category->slug;
            $data['category_id'] = $category->category_id;
            $data['serial_no'] = $serial_no;
            $data['is_published'] = 1;
            $data['is_show_menu'] = 1;
            Category::create($data);
        }
        echo 'successfully Done!!';
    }

    //119436

    public function migrateNews()
    {
        $newsContents = DB::select("SELECT 
            p.ID id,
            p.post_date published_time,
            p.post_title title,
            p.post_name slug,
            p.post_content description
        FROM
            wp_eqbw_posts p
        WHERE
            p.post_type = 'post'
                AND p.post_status = 'publish' order by id"
        );

        foreach ($newsContents as $news) {
            $data['id'] = $news->id;
            $data['title'] = $news->title;
            $data['short_description'] = $news->title;
            $data['slug'] = $news->slug;
            $data['description'] = $news->description;
            $data['reporter_id'] = 1;
            $data['publisher_id'] = 1;
            $data['published_time'] = $news->published_time;
            $data['is_video'] = 2;
            $data['is_published'] = 1;
            $data['created_by'] = 1;
            News::create($data);
        }

        echo 'successfully done!!';

    }

/*    public function migrateImage()
    {
        $file = fopen('D:\laragon7\www\obiroto_v1\storage\app\news_image.csv', "r");
        while (($data = fgetcsv($file)) !== FALSE) {
            $id = $data[0];
            if (filter_var($id, FILTER_VALIDATE_INT) === false) {
                echo $id . " -- Your variable is not an integer</br>";
            } else {
                $imageLink = explode('|', $data[1]);
                $image = $imageLink[0];
                $imageLink = public_path('wp-content/uploads/2014/06/Crow-myFBCover-1679.jpg');
                $downloadImage = $this->downloadImage($imageLink);
                dd($downloadImage);
                $path = public_path() . "/uploads/news/$id" . '/';
                $imageData['highlighted_image'] = ImageUploadRepo::uploadImage($path, $downloadImage, '260-162');
                $imageData['list_image'] = ImageUploadRepo::uploadImage($path, $downloadImage, '120-110');
                $imageData['feature_image'] = ImageUploadRepo::uploadImage($path, $downloadImage, '545-345');
                $imageData['details_image'] = ImageUploadRepo::uploadImage($path, $downloadImage, '830-400');
                NewsImage::create($imageData);
            }
        }
        fclose($file);
    }

    public function downloadImage($path)
    {
        define('MULTIPART_BOUNDARY', '--------------------------' . microtime(true));
        $header = 'Content-Type: multipart/form-data; boundary=' . MULTIPART_BOUNDARY;
        define('FORM_FIELD', 'uploaded_file');

        $filename = $path;
        $file_contents = file_get_contents($filename);

        $content =  "--".MULTIPART_BOUNDARY."\r\n".
            "Content-Disposition: form-data; name=\"".FORM_FIELD."\"; filename=\"".basename($filename)."\"\r\n".
            "Content-Type: application/zip\r\n\r\n"."Content-Type: image/jpeg\r\n\r\n".
            $file_contents."\r\n";

        $content .= "--" . MULTIPART_BOUNDARY . "--\r\n";

        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => $header,
                'content' => $content,
            )
        ));
        $imageDownload = file_get_contents('', false, $context);
        return $imageDownload;
    }*/

       public function migrateImage()
        {
            $file = fopen('D:\laragon7\www\obiroto_v1\storage\app\news_image.csv', "r");
            while (($data = fgetcsv($file)) !== FALSE) {
                $id = $data[0];
                if (filter_var($id, FILTER_VALIDATE_INT) === false ) {
                    echo $id." -- Your variable is not an integer</br>";
                } else {
                    $imageLink = explode('|', $data[1]);
                    $imageData['news_id'] = $id;
                    $linkImage = $imageLink[0];
                    $imageData['highlighted_image'] = $linkImage;
                    $imageData['list_image'] = $linkImage;
                    $imageData['feature_image'] = $linkImage;
                    $imageData['details_image'] = $linkImage;
                    NewsImage::create($imageData);
                }
            }
            fclose($file);
        }

    public function migrateRelation()
    {
        $relationslDatas = DB::select("SELECT 
		tr.object_id news_id, car.term_id category_id  
		FROM `wp_eqbw_term_relationships` tr 
        INNER JOIN
                wp_eqbw_term_taxonomy car ON tr.term_taxonomy_id = car.term_taxonomy_id
        WHERE 
                car.taxonomy = 'category' limit 8000,16000");

        //dd($relationslDatas);
        foreach ($relationslDatas as $data) {
            DB::table('category_news')->insert(
                ['news_id' => $data->news_id, 'category_id' => $data->category_id]
            );
        }
        echo 'successfully done!!';
    }


    public function uploadImage(Request $request)
    {
        $funcNum = $request->input('CKEditorFuncNum');
        $message = $url = '';
        if ($request->hasFile('upload')) {
            $file = $request->file('upload');
            if ($file->isValid()) {
                $filename = rand(1000, 9999) . $file->getClientOriginalName();
                $file->move(public_path() . '/uploads/ckeditor/', $filename);
                $url = url('uploads/ckeditor/' . $filename);
            } else {
                $message = 'An error occurred while uploading the file.';
            }
        } else {
            $message = 'No file uploaded.';
        }
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url');</script>";
    }

    public function loadSubCategory(Request $request)
    {
        $categoryIds = $request->category_ids;
        $subcategories = Category::whereIN('category_id', $categoryIds)->orderBy('serial_no', 'desc')->get();
        $options = '';
        foreach ($subcategories as $subcategory) {
            $options .= "<option value='$subcategory->id'>$subcategory->title</option>";
        }
        return response()->json(['schema' => $options]);
    }

    public function migrateImageHttps(Request $request)
    {
        $newsImages = NewsImage::skip($request->skip)->take($request->take)->get();
        foreach ($newsImages as $image) {
            $data = [];
            if (strpos($image->list_image, 'http://www.obiroto.com/') !== false) {
                $data['list_image'] = str_replace('http://www.obiroto.com/','https://www.obiroto.com/', $image->list_image);
            }
            if (strpos($image->feature_image, 'http://www.obiroto.com/') !== false) {
                $data['feature_image'] = str_replace('http://www.obiroto.com/','https://www.obiroto.com/', $image->feature_image);
            }
            if (strpos($image->details_image, 'http://www.obiroto.com/') !== false) {
                $data['details_image'] = str_replace('http://www.obiroto.com/','https://www.obiroto.com/', $image->details_image);
            }
            if (strpos($image->highlighted_image, 'http://www.obiroto.com/') !== false) {
                $data['highlighted_image'] = str_replace('http://www.obiroto.com/','https://www.obiroto.com/', $image->highlighted_image);
            }
            $image->update($data);
        }

        echo 'done';
    }

}