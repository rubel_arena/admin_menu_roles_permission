<?php

namespace App\Http\Controllers\Admin;

use App\Events\NewsBrodCast;
use App\Helpers\CommonHelper;
use App\Helpers\Tagging;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsRequest;
use App\Models\Category;
use App\Models\Tag;
use App\Repositories\ImageUploadRepo;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Models\News;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the News.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where(['is_published' => 1])->orderBy('serial_no')->get()->pluck('title', 'id')->prepend('All', '');

        //$news = News::with('categories', 'user')->withCount('comments')->orderBy('id', 'desc')->get();
        return View('admin.news.index', [
            // 'news' => $news,
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new news.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where(['is_published' => 1,'category_id' => 0])->orderBy('serial_no')->get()->pluck('title', 'id');
        $tags = Tag::get()->pluck('title', 'id');
        $sub_categories = [
            '' => 'Select Category First',
        ];
        $published_status = [
            1 => 'Published',
            2 => 'Not Published'
        ];
        return view('admin.news.form', compact('categories', 'published_status','sub_categories','tags'));
    }

    /**
     * Store a newly created news in database.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request)
    {
        //manage tage first
        $tagsIs = Tagging::processTag($request->tag_id);
        $category_ids = $request->category_id;
        $subCategoryIds = $request->sub_category_id;
        if (empty($subCategoryIds)) {
            $margeCategoryIds = $category_ids;
        } else {
            $margeCategoryIds = array_merge($category_ids, $subCategoryIds);
        }
        $userId = auth()->id();
        $newsData = $request->except('category_id', 'image', 'sub_category_id','tag_id');
        $newsData['reporter_id'] = $userId;
        $newsData['publisher_id'] = ($request->is_published === '1') ? $userId : null;
        $newsData['published_time'] = ($request->is_published === '1') ? Carbon::now() : null;
        $newsData['ip_address'] = CommonHelper::getRealIpAddr();
        $newsData['created_by'] = $userId;
        $news = News::create($newsData);

        /*broadcast a event end*/

        if (count($tagsIs) > 0) {
            $news->tags()->sync($tagsIs);
        }

        $news->categories()->sync($margeCategoryIds);

        if ($request->hasFile('image')) {
            $image = $request->image;
            $path = public_path() . "/uploads/news/$news->id" . '/';
            $imageData['highlighted_image'] = ImageUploadRepo::uploadImage($path, $image, '260-162');
            $imageData['list_image'] = ImageUploadRepo::uploadImage($path, $image, '120-110');
            $imageData['feature_image'] = ImageUploadRepo::uploadImage($path, $image, '545-345');
            $imageData['details_image'] = ImageUploadRepo::uploadImage($path, $image, '830-400');
            $news->image()->create($imageData);
        }

        /*broadcast a event start*/
        broadcast(new NewsBrodCast($news->id))->toOthers();

        Session::flash('message', Lang::get('messages.Saved successfully'));
        return redirect()->route('news.index');
    }

    /**
     * Display the specified news.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'hello';
    }

    /**
     * Show the form for editing the specified news.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $tags = Tag::get()->pluck('title', 'id');
        $categories = Category::where(['is_published' => 1,'category_id' => 0])->orderBy('serial_no')->get()->pluck('title', 'id');
        $allCategoryIds = collect(DB::table('category_news')->where('news_id', $news->id)->get())->pluck(['category_id'])->toArray();
        $category_ids = Category::whereIn('id', $allCategoryIds)->where('category_id', 0)->get()->pluck(['id'])->toArray();
        $sub_category_ids = Category::whereIn('id', $allCategoryIds)->where('category_id','!=' , 0)->get()->pluck(['id'])->toArray();
        $sub_categories = Category::whereIn('category_id',$category_ids)->where('is_published' ,1)->where('category_id','!=', 0)->orderBy('serial_no')->get()->pluck('title', 'id');
        $tag_ids = Tag::whereHas('news', function ($q) use($news){
            $q->where('id', $news->id);
        })->get()->pluck(['id']);
        $published_status = [
            1 => 'Published',
            2 => 'Not Published'
        ];
        return view('admin.news.form')
            ->with(['item' => $news, 'categories' => $categories, 'published_status' => $published_status, 'category_ids' => $category_ids,
                'sub_category_ids'=> $sub_category_ids, 'sub_categories' => $sub_categories,'tags' => $tags, 'tag_ids' => $tag_ids]);
    }

    /**
     * Update the specified news in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $tagsIs = Tagging::processTag($request->tag_id);
        $category_ids = $request->category_id;
        $subCategoryIds = $request->sub_category_id;
        if (empty($subCategoryIds)) {
            $margeCategoryIds = $category_ids;
        } else {
            $margeCategoryIds = array_merge($category_ids, $subCategoryIds);
        }
        $userId = auth()->id();
        $newsData = $request->except('category_id', 'image','sub_category_id','tag_id');
        $newsData['reporter_id'] = $userId;
        $newsData['publisher_id'] = ($news->is_published !== 1 && $request->is_published == 1) ? $userId : $news->publisher_id;
        $newsData['published_time'] = ($news->is_published !== 1 && $request->is_published == 1) ? Carbon::now() : null;
        $newsData['ip_address'] = CommonHelper::getRealIpAddr();
        $newsData['updated_by'] = $userId;
        $news->update($newsData);

        if (count($tagsIs) > 0) {
            $news->tags()->sync($tagsIs);
        }

        $news->categories()->sync($margeCategoryIds);

        if ($request->hasFile('image')) {
            $existiongImage = $news->image;
            $image = $request->image;
            $path = public_path() . "/uploads/news/$news->id" . '/';
            //$imageData['news_id'] = $news->id;
            $imageData['highlighted_image'] = ImageUploadRepo::uploadImage($path, $image, '260-162', $existiongImage->highlighted_image);
            $imageData['list_image'] = ImageUploadRepo::uploadImage($path, $image, '120-110', $existiongImage->list_image);
            $imageData['feature_image'] = ImageUploadRepo::uploadImage($path, $image, '545-345', $existiongImage->feature_image);
            $imageData['details_image'] = ImageUploadRepo::uploadImage($path, $image, '830-400', $existiongImage->details_image);
            $news->image()->update($imageData);

        }

        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('news.index');
    }

    /**
     * Remove the specified news from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->categories()->detach();
        $news->delete();
        Session::flash('message', Lang::get('messages.Deleted successfully'));
        return redirect()->route('news.index');
    }

    /**
     * Datatable Ajax fetch
     *
     * @return
     */

    public function newsPublished($id)
    {
        $userId = auth()->id();
        $news = News::where('id', $id)->first();
        $news->is_published = 1;
        $news->updated_by = $userId;
        $news->publisher_id = $userId;
        $news->published_time = Carbon::now();
        $news->save();
        Session::flash('message', Lang::get('messages.Updated successfully'));
        return redirect()->route('news.index');
    }

    public function newsLoadByAjax(Request $request)
    {
        $categoryId = $request->category_id;
        $limit = $request->length;
        $startoffset = $request->start;
        $searchValue = $request->search['value'];

        //DB::connection()->enableQueryLog();
        $totalRow = News::whereHas('categories', function ($category) use ($categoryId) {
            if (is_null($categoryId)) {
                $category->where('id', '!=', 0);
            } else {
                $category->where('id', $categoryId);
            }
        })->where('title','LIKE','%'.$searchValue.'%')->orderBy('id', 'desc')->get();
        //dd(DB::getQueryLog());

        $newsCollections = News::whereHas('categories', function ($category) use ($categoryId) {
            if (is_null($categoryId)) {
                $category->where('id', '!=', 0);
            } else {
                $category->where('id', $categoryId);
            }
        })->where('title','LIKE','%'.$searchValue.'%')->withCount('comments')->orderBy('id', 'desc')->skip($startoffset)->take($limit)->get();

        $data = [];
        if (count($newsCollections) > 0) {
            foreach ($newsCollections as $key => $row) {
                $categories = '';
                foreach ($row->categories as $category) {
                    $categories .= "<span class='badge badge-info'>$category->title</span>";
                }

                $editUrl = url(config('obiroto.adminRoute') . '/news/' . $row->id . '/edit');
                $deleteUrl = url(config('obiroto.adminRoute') . '/news/' . $row->id);
                $commentsUrl = url(config('obiroto.adminRoute') . '/news/' . $row->id . '/comments');
                $publishedUrl = route('admin.news.published', $row->id);

                $nestedData['seralNo'] = ++$startoffset;
                $nestedData['title'] = Str::limit($row->title, 50);
                $nestedData['category'] = $categories;
                $nestedData['status'] = $row->is_published == 1 ? "<span class='badge badge-success'>Published</span>" : "<span class='badge badge-warning'>Not Published</span>";
                $nestedData['createdBy'] = $row->user->name;
                $nestedData['publishedTime'] = !empty($row->published_time) ? $row->published_time : '';
                $nestedData['action'] = "
                    <a href='$editUrl' class='btn btn-warning btn-xs' data-toggle='tooltip' title='Edit'> 
                           <i class='fa fa-edit'></i>
                     </a> 
                     <a href='$commentsUrl' class='btn btn-info btn-xs' data-toggle='tooltip' title='Comments'> 
                           <span class='badge badge-danger'>&nbsp; $row->comments_count</span><i class='fa fa-comments'></i>

                     </a>
                    ";
                if ($row->is_published == 2) {
                    $token = csrf_token();
                    $nestedData['action'] .= "
                     <a href='$publishedUrl' class='btn btn-warning btn-xs' data-toggle='tooltip' title='Need For Approval' onclick=\"return confirm('Are you sure to published this news?')\"> 
                           Published
                     </a> 
                    <form method='post' action='$deleteUrl' style='display: inline'> 
                       <input type='hidden' name='_token' id='csrf-token' value=$token />
                       <input type='hidden' name='_method' value='delete'>
                        <button class=\"btn btn-danger btn-xs text-white\" data-toggle=\"tooltip\" title=\"Delete\"
                                                 onclick=\"return confirm('Are you sure to delete this?')\"><i class=\"fas fa-times\"></i>
                       </button>
                    </form>
              
                    ";
                }
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->draw),
            "recordsTotal" => count($totalRow),
            "recordsFiltered" => count($totalRow),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public static function inboxesData($category_id, $limit_string, $total = false)
    {
        if ($total) {
            $limit_string = "";
        }
        $list_values = DB::SELECT("SELECT 
            n.id, n.title, n.is_published, n.description, n.created_by, n.published_time, cn.category_id, cn.news_id
        FROM
            news n
                INNER JOIN
            category_news cn ON n.id = cn.news_id
        WHERE
            cn.category_id = $category_id
            order by n.id desc $limit_string");

        return $list_values;
    }
}
