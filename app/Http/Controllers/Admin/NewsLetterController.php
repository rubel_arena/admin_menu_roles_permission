<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsLetterRequest;
use App\Mail\NewsLetter;
use App\Models\Ad;
use App\Models\Category;
use App\Models\News;
use App\Models\NewsLetterHistory;
use App\Models\Setting;
use App\Models\Subscriber;
use Carbon\Carbon;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class NewsLetterController extends Controller
{
    public function index()
    {
        $categories = Category::where('is_published', 1)->orderBy('serial_no')->get()->pluck('title', 'id');
        return view('admin.news_letter.form')->with(['categories' => $categories]);
    }

    public function loadNewsSchema(Request $request)
    {
        if ($request->ajax()) {
            $categoryIds = $request->category_ids;

            $categories = Category::with(['news' => function ($q) {
                $q->select('id', 'title', 'published_time')->where('is_published', 1)->whereBetween('published_time', [Carbon::now()->startOfWeek(Carbon::FRIDAY), Carbon::now()->endOfWeek(Carbon::THURSDAY)]);
            }])->whereIn('id', $categoryIds)->get();
            if (count($categories) > 0) {
                $data = view('admin.news_letter.load_news_schema', compact('categories'))->render();
                return response()->json(['schema' => $data]);
            } else {
                return response()->json(['schema' => '0']);
            }
        }
    }

    public function storeNewsLetter(NewsLetterRequest $request)
    {
        $newsIds = $request->news;
        $newsList = News::with('image', 'categories')->whereIn('id', $newsIds)->get();
        $setting = Setting::first();
        $ad = Ad::find(9);
        $interviews = News::whereHas('categories', function ($category) {
            $category->where('categories.serial_no', 3);
        })->with(['image', 'categories'])
            ->where('is_published', 1)
            ->whereBetween('published_time', [Carbon::now()->startOfWeek(Carbon::FRIDAY), Carbon::now()->endOfWeek(Carbon::THURSDAY)])
            ->get();
        $categoriesNews = Category::with(['news' => function($q) {
            $q->with(['image', 'categories'])
                ->where('is_published', 1)
                ->take(3)
                ->whereBetween('published_time', [Carbon::now()->startOfWeek(Carbon::FRIDAY), Carbon::now()->endOfWeek(Carbon::THURSDAY)]);
        }])
            ->where('serial_no', '!=' , 3)
            ->take(3)
            ->orderBy('serial_no', 'asc')
            ->get();
        $subscribers = Subscriber::where('is_verified', 1)->get();
        $subscriber_ids = $subscribers->pluck('id')->toArray();
        $subject = $request->subject;
        if (count($subscribers) > 0 && count($newsList) > 0) {
            $data['subscriber_id'] = implode(',', $subscriber_ids);
            $data['news_id'] = implode(',', $newsIds);
            $data['send_by'] = auth()->id();
            $data['note'] = $request->note;
            $data['ip_address'] = CommonHelper::getRealIpAddr();
            NewsLetterHistory::create($data);
            foreach ($subscribers as $subscriber) {
                Mail::to($subscriber->email)->send(new NewsLetter($newsList, $setting, $ad, $interviews, $categoriesNews,$subject));
            }

            Session::flash('message', 'Successfully Send');
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors('News or Subscriber not found!!');
        }
    }
}
