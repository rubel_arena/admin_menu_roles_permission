<?php

namespace App\Http\Controllers\API\V1;

use App\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\CommentStoreRequest;
use App\Http\Requests\API\SubscribeRequest;
use App\Http\Requests\API\UserLoginRequest;
use App\Http\Requests\API\UserRegisterRequest;
use App\Models\Ad;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Gallery;
use App\Models\Interview;
use App\Models\Latest_Job;
use App\Models\News;
use App\Models\Setting;
use App\Models\Subscriber;
use App\Models\Tag;
use App\Models\User;
use App\Models\View;
use App\Repositories\BanglaDateTodayRepo;
use App\Repositories\CommonRepo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Mail;

class FrontEndController extends Controller
{
    public function getAds()
    {
        $ads = Ad::all();
        return response()->json($ads);
    }

    public function newsDetails($id)
    {
        /*view data insert*/
        $view = View::where(['viewable_type' => 'App\Models\News', 'viewable_id' => $id])->first();
        if (is_null($view)) {
            $data['viewable_type'] = 'App\Models\News';
            $data['viewable_id'] = $id;
            $data['counter'] = 1;
            $data['ip_address'] = CommonHelper::getRealIpAddr();
            $data['read_at'] = Carbon::now();
            View::create($data);
        } else {
            $view->counter += 1;
            $view->save();
        }

        $details = News::with(['image','categories', 'comments' => function ($q) {
            $q->with(['user' => function ($q) {
                $q->select('id', 'name');
            }])->where('is_published', 1);
        }, 'tags'])->where(['is_published' => 1, 'id' => $id])->first();
        return response()->json($details);
    }

    public function newsDetailsSlug($slug)
    {
        $details = News::with(['image', 'comments' => function ($q) {
            $q->with(['user' => function ($q) {
                $q->select('id', 'name');
            }])->where('is_published', 1);
        }])->where(['is_published' => 1, 'slug' => urlencode($slug)])->first();
        return response()->json($details);
    }


    /* public function getNews($serial_no, Request $request)
     {
         $news = News::select('id','title','short_description','published_time')->whereHas('categories', function ($category) use ($serial_no) {
             $category->where('categories.serial_no', $serial_no);
         })->with(['image', 'categories' => function ($category) use ($serial_no) {
             $category->where('categories.serial_no', $serial_no);
         }])->withCount('comments')->where(['is_published' => 1])
             ->orderBy('id', 'desc')
             ->skip($request->skip)
             ->take($request->take)
             ->get();

         return response()->json($news);
     }*/

    public function getNews($serial_no, Request $request)
    {
        $news = News::select('id', 'title', 'slug', 'short_description', 'published_time')->whereHas('categories', function ($category) use ($serial_no) {
            $category->where('categories.serial_no', $serial_no);
        })->with(['image', 'categories' => function ($category) use ($serial_no) {
            $category->where('categories.serial_no', $serial_no);
        }])->withCount('comments')->where(['is_published' => 1])
            ->orderBy('id', 'desc')
            ->skip($request->skip)
            ->take($request->take)
            ->get();

        return response()->json($news);
    }

    public function latestNews(Request $request)
    {
        $news = News::select('id', 'title', 'slug')->with('categories')->where(['is_published' => 1])
            ->orderBy('id', 'desc')
            ->take($request->take)
            ->get();

        return response()->json($news);
    }

    public function categoryNewsList($category_slug, Request $request)
    {
        $news = News::select('id', 'title', 'slug', 'short_description', 'published_time')->whereHas('categories', function ($category) use ($category_slug) {
            $category->where('categories.slug', $category_slug);
        })->with(['image', 'categories' => function ($category) use ($category_slug) {
            $category->where('categories.slug', $category_slug);
        }])->where(['is_published' => 1])
            ->orderBy('id', 'desc')
            ->paginate(5);

        return response()->json($news);
    }

    public function relatedNews($category_slug, $except, Request $request)
    {
        $news = News::whereHas('categories', function ($category) use ($category_slug) {
            $category->where('categories.slug', $category_slug);
        })->with(['image', 'categories' => function ($category) use ($category_slug) {
            $category->where('categories.slug', $category_slug);
        }])->withCount('comments')->where(['is_published' => 1])
            ->whereNOTIn('id', [$except])
            ->orderBy('id', 'desc')
            ->take($request->take)
            ->get();

        return response()->json($news);
    }

    public function archiveNews(Request $request)
    {
        $news = News::with('categories')->withCount('comments')
            ->where('published_time', '<', Carbon::now()->subDays(7)->toDateTimeString())
            ->take($request->take)
            ->orderBy('id', 'desc')
            ->get();
        return response()->json($news);
    }

    public function popularNews(Request $request)
    {
        $news = News::with(['image', 'categories'])->whereHas('views', function ($query) {
            $query->orderBy('counter', 'desc');
        })
            ->where(['is_published' => 1])
            ->take($request->take)
            ->get();
        return response()->json($news);
    }

    public function galleries(Request $request)
    {
        $galleries = Gallery::orderBY('serial_no', 'asc')
            ->take($request->take)
            ->get();
        return response()->json($galleries);
    }

    public function latestJobs(Request $request)
    {
        $jobs = Latest_Job::where('is_published', 1)->latest()->take($request->take)->get();
        return response()->json($jobs);
    }

    public function latestJobsDetail($id)
    {
        $job = Latest_Job::where('is_published', 1)->find($id);
        return response()->json($job);
    }

    public function interviews(Request $request)
    {
        $interviews = Interview::where('status', 1)->latest()
            ->take($request->take)
            ->get();
        return response()->json($interviews);
    }

    public function interviewDetail($id)
    {
        $detail = Interview::find($id);
        return response()->json($detail);
    }


    public function getCategories()
    {
        $categoriesList = DB::table('categories')->where(['category_id' => 0, 'is_show_menu' => 1])->orderBy('order_no')->get();
        $categories = array();
        foreach ($categoriesList as $row) {
            $categories[] = array(
                'id' => $row->id,
                'parent_id' => $row->category_id,
                'category_name' => $row->title,
                'slug' => $row->slug,
                'subcategory' => $this->sub_categories($row->id),
            );
        }

        return response()->json($categories);
    }

    function sub_categories($id)
    {
        $subCategoriesList = DB::table('categories')->where(['category_id'=> $id, 'is_show_menu' => 1])->orderBy('order_no')->get();

        $subCategories = array();

        foreach ($subCategoriesList as $row) {
            $subCategories[] = array(
                'id' => $row->id,
                'parent_id' => $row->category_id,
                'category_name' => $row->title,
                'slug' => $row->slug,
                'subcategory' => $this->sub_categories($row->id),
            );
        }

        return $subCategories;
    }


    public function userRegister(UserRegisterRequest $request)
    {
        try {
            $password = $request->password;
            $data = $request->except('password');
            $data['password'] = bcrypt($password);
            $data['verify_token'] = Str::random(40);
            $data['is_verified'] = 0;
            $data['user_level'] = 1;
            $user = User::create($data);

            Mail::send('emails.verify_email', ['user' => $user, 'password' => $password], function ($m) use ($user) {
                $setting = Setting::first();
                $m->to($user->email)->from($setting->email, $setting->brand_name)->subject('Register Mail Verification from Obiroto!');
            });

            return response()->json([
                'status' => 'success',
                'msg' => 'Okay',
            ], 201);
        } catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }
    }

    public function login(UserLoginRequest $request)
    {
        $email = $request->email;
        $password = $request->password;

        $user = User::where('email', '=', $email)->first();
        if (!$user) {
            return response()->json(['success' => false, 'message' => 'You are not register.']);
        }
        if (!Hash::check($password, $user->password)) {
            return response()->json(['success' => false, 'message' => 'User Name Or Password Don\'t match']);
        }
        if ($user->is_verified == 0) {
            return response()->json(['success' => false, 'message' => 'Your email not verified']);
        }
        return response()->json(['success' => true, 'message' => 'success', 'data' => $user]);
    }

    public function storeComment(CommentStoreRequest $request)
    {
        try {
            $comment = Comment::create([
                'comment' => $request->comment,
                'user_id' => $request->user_id,
                'commentable_type' => "App\Models\News",
                'commentable_id' => $request->news_id,
                'is_published' => 2
            ]);

            return response()->json([
                'status' => 'success',
                'msg' => 'Okay',
            ], 201);
        } catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }
    }

    public function categoryWiseNews()
    {
        $categories = Category::select('id', 'title', 'slug', 'serial_no')->with(['news' => function ($news) {
            $news->select('news.id', 'title', 'slug', 'description', 'published_time', 'short_description')->withCount('comments');
        }, 'news.image', 'news.categories'])
            ->where('category_id', 0)
            ->orderBy('serial_no')
            ->get();

        $newCategories = $categories->reduce(function ($newCategories, $category) {
            $newCategories[$category['slug']] = $category;
            return $newCategories;
        }, []);

        return response()->json($newCategories, 200);
    }

    public function setting()
    {
        $todayBanglaDate = (new BanglaDateTodayRepo(time()))->get_date();
        $todayEnglish = CommonRepo::timestampToBanglaDate(now(), 'd F Y');
        $setting = Setting::first();
        $setting->setAttribute('todayBanglaDate', $todayBanglaDate);
        $setting->setAttribute('todayEnglishDate', $todayEnglish);
        return response()->json($setting, 200);
    }

    public function search(Request $request)
    {
        $searchValue = str_replace('-', ' ', $request->q);
        $results = News::with(['image' => function ($q) {
            $q->select('id', 'news_id', 'list_image');
        }, 'categories'])->where('title', 'LIKE', '%' . $searchValue . '%')
            ->orWhere('description', 'LIKE', '%' . $searchValue . '%')
            ->limit(10)
            ->get(['id', 'title', 'slug', 'short_description', 'published_time']);
        return response()->json($results, 200);
    }

    public function subscribe(SubscribeRequest $request)
    {
        try {
            $subscriber = Subscriber::create([
                'email' => $request->email,
                'is_verified' => 2,
                'verify_token' => Str::random(40)
            ]);

            $subscriber = Subscriber::findOrFail($subscriber->id);

            Mail::send('emails.verify_subscriber_email', ['user' => $subscriber], function ($m) use ($subscriber) {
                $setting = Setting::first();
                $m->to($subscriber->email)->from($setting->email, $setting->brand_name)->subject('Subscribed Mail Verification from Obiroto!');
            });

            return response()->json([
                'status' => 'success',
                'msg' => 'Okay',
            ], 201);
        } catch (ValidationException $exception) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Error',
                'errors' => $exception->errors(),
            ], 422);
        }
    }


    public function subscriber_verification($verify_token)
    {
        $user = Subscriber::where(['verify_token' => $verify_token])->first();
        if (!empty($user)) {
            $user->is_verified = 1;
            $user->verify_token = null;
            $user->save();
            $message = 'success';
        } else {
            $message = "errors";
        }
        return response()->json($message, 200);
    }

    public function user_verification($verify_token)
    {
        $user = User::where(['verify_token' => $verify_token])->first();
        if (!empty($user)) {
            $user->is_verified = 1;
            $user->verify_token = null;
            $user->save();
            $message = $user;
        } else {
            $message = "errors";
        }
        return response()->json($message, 200);
    }

    public function socialAttempt(Request $request)
    {
        $email = $request->email;
        $user = User::where('email', $email)->first();
        if (!$user) {
            $data = $request->all();
            $data['verify_token'] = null;
            $data['is_verified'] = 1;
            $data['user_level'] = 1;
            $user = User::create($data);
        }

        return response()->json($user, 200);
    }

    public function newsDependOnTag($slug)
    {
        $news = News::select('id','title', 'slug', 'short_description','published_time')->with(['image' => function($q) {
            $q->select('id','news_id', 'details_image','list_image');
        }, 'categories' => function($q) {
            $q->select('id', 'title', 'slug');
        }])->whereHas('tags', function ($q) use($slug){
            $q->where('slug', $slug);
        })->where('is_published',1)
            ->paginate(5);
        return response()->json($news, 200);
    }

}
