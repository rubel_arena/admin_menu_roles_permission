<?php

namespace App\Providers;

use App\Models\Menu;
use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $setting = Setting::first();
        $menues = Menu::with(['subMenues' => function ($q) {
            $q->select('id', 'parent_id', 'menu_name', 'slug');
        }])
            ->whereNull('parent_id')
            ->orderBy('id', 'asc')
            ->get(['id', 'menu_name','is_parent', 'slug', 'has_child']);
        //dd($menues);
        view()->share(['setting' => $setting, 'menues' => $menues]);
        Schema::defaultStringLength(191);
    }
}
