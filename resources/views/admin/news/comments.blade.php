@extends('layouts.master')

@section('css')
    <!-- datatables css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('breadCurmbs')@show

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                           style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Comments</th>
                            <th class="text-center">Status</th>
                            <th style="cursor: pointer" class="text-center">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="selectall" id="selectall" class="select">
                                        Select All
                                    </label>
                                </div>

                            </th>
                        </tr>
                        </thead>

                        {!! Form::open(['route' => ["comments.bulk.activation"], 'method'=>'POST','role'=>"form",'id' => 'add-form']) !!}
                        <tbody>
                        @if(count($news->comments) > 0)
                            @foreach($news->comments as $key=>$item)
                                <tr class="row1" data-id="{{ $item->id }}">
                                    <td>{{++$key}}</td>
                                    <td>{{$item->comment}}</td>
                                    <td class="text-center">
                                        @if($item->is_published == 1)
                                            <span class="badge badge-success">Published</span>
                                        @elseif($item->is_published == 2)
                                            <span class="badge badge-warning">Not Published</span>
                                        @else
                                            <span class="badge badge-danger">Canceled</span>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <div class="checkbox">
                                            <label><input name="comments[]" type="checkbox" class="select"
                                                          value="{{$item->id}}"></label>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            <tr style="background-color: transparent">
                                <td colspan="3"></td>
                                <td width="220px" class="text-center">
                                    <div class="form-group">
                                        {!! Form::select('is_published', $published_status, isset($item->is_published) ? $item->is_published : 2, ['class' => 'form-control', 'required'=>'required']) !!}
                                    </div>
                                    <button type="submit" class="submit btn btn-primary" id="send">Submit</button>
                                    <button class="reset btn btn-warning" type="reset" id="reset" name="approve">Reset
                                    </button>
                                </td>
                            </tr>
                        @endif

                        </tbody>
                    </table>
                    {{ Form::close() }}

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>

    <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

    <script>
        $(function () {
            /*Bulk Activation start*/
            $("#selectall").click(function () {
                $(".select").prop("checked", $("#selectall").prop("checked"))
            });

            $("#reset").click(function () {
                $(".select").attr("checked", false);
            });
        })
    </script>

@endsection