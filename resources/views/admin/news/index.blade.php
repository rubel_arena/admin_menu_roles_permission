@extends('layouts.master')

@section('css')
    <!-- datatables css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        div.dataTables_wrapper div.dataTables_filter {
            text-align: right;
            display: inline;
            float: right;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('category_id', 'Categories') !!}
                <span class="la-required">*</span>
                {!! Form::select('category_id', $categories, isset($category_ids) ? $category_ids : null, ['class' => 'form-control select2', 'id' => 'category', 'required'=>'required']) !!}
                {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="">&nbsp;</label>
                <button type="button" class="btn btn-info btn-block" id="search">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    Search
                </button>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="datatable" class="table table-bordered dt-responsive nowrap"
                               style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>@lang('messages.serial_no')</th>
                                <th>@lang('messages.Title')</th>
                                <th>@lang('messages.Category')</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Published Time</th>
                                <th class="text-center" width="200px">@lang('messages.Actions')</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    @endsection

    @section('script')

        <!-- Plugins js -->
            <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
            <script src="{{ URL::asset('assets/libs/jszip/jszip.min.js')}}"></script>
            <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
            <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>

            <script>
                $(function () {
                    $('#category').select2();
                    const category_id = document.getElementById('category').value;

                   fill_datatable(category_id);
                    function fill_datatable(category_id) {
                        var myTable = $('#datatable').DataTable({
                            "processing": true,
                            "serverSide": true,
                            "ordering": false,
                            "searching": true,
                            "dom": '<"top"Bf>rt<"bottom"lip><"clear">',
                            buttons: [
                                'csv', 'excel', 'pdf', 'print'
                            ],
                            "ajax": {
                                type: "POST",
                                dataType: "json",
                                url: "{{ url(config('obiroto.adminRoute').'/get-news') }}",
                                data: {
                                    category_id: category_id,
                                    _token: '{{csrf_token()}}'
                                }
                            },
                            "columns": [
                                {"data": "seralNo"},
                                {"data": "title"},
                                {"data": "category"},
                                {"data": "status"},
                                {"data": "createdBy"},
                                {"data": "publishedTime"},
                                {"data": "action"}
                            ]
                        });
                    }
                    $('#search').on('click', function () {
                        const category_id = document.getElementById('category').value;
                        //alert(category_id);
                        $('#datatable').DataTable().destroy();
                        fill_datatable(category_id);
                    });
                });
            </script>

@endsection