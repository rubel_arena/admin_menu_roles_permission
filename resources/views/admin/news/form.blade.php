@extends('layouts.master')

@section('css')
    <!-- Plugin css -->
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            @if(isset($item))
                {!! Form::model($item, ['route' => ["news.update", $item->id],'method' =>'PUT', 'class' => 'custom-validation', 'files' => true,'role'=>"form", 'id' => 'edit-form']) !!}
            @else
                {!! Form::open(['route' => ["news.store"], 'method'=>'POST', 'class' => 'custom-validation', 'files' => true, 'role'=>"form",'id' => 'add-form']) !!}
            @endif
            <form class="custom-validation" action="#">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('category_id', 'Categories') !!}
                            <span class="la-required">*</span>
                            {!! Form::select('category_id[]', $categories, isset($category_ids) ? $category_ids : null, ['class' => 'form-control select2 select2-multiple', 'id' => 'category', 'required'=>'required','multiple' => 'multiple']) !!}
                            {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('sub_category_id', 'Sub Categories') !!}
                            {!! Form::select('sub_category_id[]', $sub_categories, isset($sub_category_ids) ? $sub_category_ids : null, ['class' => 'form-control select2 select2-multiple', 'id' => 'sub_category','multiple' => 'multiple']) !!}
                            {!! $errors->first('sub_category_id', '<p class="help-block">:message</p>') !!}

                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('title', trans('messages.title')) !!} <span class="la-required">*</span>
                            {!! Form::text('title', null,['class'=>'form-control', 'placeholder'=>trans('messages.enter_title'), 'required'=>'required']) !!}
                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('reporter_info', 'Reporter Info') !!}
                            {!! Form::text('reporter_info', null,['class'=>'form-control', 'placeholder'=>'Reporter Info']) !!}
                            {!! $errors->first('reporter_info', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            {!! Form::label('short_description', trans('messages.short_description')) !!} <span
                                    class="la-required">*</span>
                            {!! Form::textarea('short_description', null,['class'=>'form-control', 'placeholder'=>trans('messages.enter_short_description'), 'required'=>'required', 'rows' => 3, 'columns' => 3]) !!}
                            {!! $errors->first('short_description', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            {!! Form::label('description', trans('messages.description')) !!} <span
                                    class="la-required">*</span>
                            {!! Form::textarea('description', null,['class'=>'form-control editors', 'placeholder'=>trans('messages.enter_description'), 'required'=>'required']) !!}
                            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('tag_id', 'Tags') !!}
                            {!! Form::select('tag_id[]', $tags, isset($tag_ids) ? $tag_ids : null, ['class' => 'form-control select2 select2-multiple', 'id' => 'tags','multiple' => 'multiple']) !!}
                            {!! $errors->first('tag_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('is_published', trans('messages.is_published')) !!} <span
                                    class="la-required">*</span>
                            {!! Form::select('is_published', $published_status, isset($item->is_published) ? $item->is_published : 2, ['class' => 'form-control', 'required'=>'required']) !!}
                            {!! $errors->first('is_published', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    @if(!isset($item))
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('images', trans('messages.images')) !!} <span
                                        class="la-required">*</span>
                                <br>
                                <label class="btn btn-success btn-cons m-b-10">
                                    <i class="fas fa-upload"></i>&nbsp <span>Upload</span>
                                    {!! Form::file('image',['style'=>"display: none;",'id'=>"image_upload"]) !!}
                                </label><br>
                                {!! $errors->first('images', '<p class="help-block">:message</p>') !!}
                                <span class="suggestion_text"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Type: jpg,png,jpeg,gif. Size:650X580px. Maximum File Size 1MB
                               </span>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="update-image-wrapper">
                        <div class="col-lg-4">
                            @if(isset($item) && isset($item->image))
                                <div class="col-lg-4">
                                    <img width="100px" height="100px" src="{{asset($item->image->list_image)}}"
                                         alt="{{$item->title or null}}">
                                </div>
                                <div class="col-lg-8">
                                    <label class="btn btn-success btn-cons m-b-10">
                                        <i class="fas fa-upload"></i>&nbsp <span>Upload</span>
                                        {!! Form::file('image',['style'=>"display: none;",'class'=>"image_upload"]) !!}
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row row-centered">
                    <div class="col-lg-10 col-centered">
                        <div id="image-preview">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group mb-0">
                            <div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                    Submit
                                </button>
                                <a href={{route('news.index')}}>
                                    <button type="button" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div> <!-- end col -->

@endsection

@section('script')
    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
//multiple select
            $('#category').select2({
                placeholder: "Select Category"
            });

            $('#sub_category').select2({
                placeholder: "Select Category First"
            });

            $('#tags').select2({
                tags : true,
                placeholder: "Select Tags"
            });

            $('#image_upload').on('change', function () {
                imagesPreview(this, 'div#image-preview', false);
            });

            $('#category').on('change', function () {
                var category_ids = $(this).val();
                console.log(category_ids);
                if (category_ids.length > 0) {
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "{{ url(config('obiroto.adminRoute').'/load-sub-category') }}",
                        data: {
                            category_ids: category_ids,
                            _token: '{{csrf_token()}}'
                        },
                        beforeSend: function () {
                            $("#loader").show();
                        },
                        success: function (data) {
                            $("#loader").hide();
                            const subcategory = "#sub_category";
                            $(subcategory).empty();
                            $(subcategory).append(data.schema);
                        }
                    });
                } else {
                    $('#sub_category').empty();
                }

            });

        });
    </script>
@endsection