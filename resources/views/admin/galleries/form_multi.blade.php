@extends('layouts.master')

@section('css')
    <!-- Plugin css -->
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            @if(isset($item))
                {!! Form::model($item, ['route' => ["gallery.update", $item->id],'method' =>'PUT', 'class' => 'custom-validation', 'files' => true,'role'=>"form", 'id' => 'edit-form']) !!}
            @else
                {!! Form::open(['route' => ["gallery.store"], 'method'=>'POST', 'class' => 'custom-validation', 'files' => true, 'role'=>"form",'id' => 'add-form']) !!}
            @endif
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Image</label>
                        <input class="form-control-file" type="file" name="images[]">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Preview</label>
                        <img src="" alt="">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Caption</label>
                        <input class="form-control" type="text" name="captions[]">
                    </div>
                </div>
                <div class="col-md-3">
                   <div class="form-group">
                       <label style="display: block">
                           &nbsp;
                       </label>
                       <button class="btn btn-warning">
                           <i class="fas fa-plus"></i>
                       </button>
                   </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                Submit
                            </button>
                            <a href={{route('gallery.index')}}>
                                <button type="button" class="btn btn-secondary waves-effect">
                                    Cancel
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div> <!-- end col -->

@endsection

@section('script')
    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script type="text/javascript">
        /*Multiple image brows after show*/
        $(function () {
            $('#image_upload').on('change', function () {
                imagesPreview(this, 'div#image-preview', false);
            });

        });
    </script>
@endsection