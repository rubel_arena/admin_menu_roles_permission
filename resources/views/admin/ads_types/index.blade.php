@extends('layouts.master')

@section('css')
    <!-- datatables css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                           style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr class="success">
                            <th>@lang('messages.Serial No.')</th>
                            <th>@lang('messages.Title')</th>
                            {{--<th>Image Quantity</th>--}}
                            <th>Image Size</th>
                            <th>Price</th>
                            <th class="text-center" width="200px">@lang('messages.Actions')</th>
                        </tr>
                        </thead>


                        <tbody id="tablecontents">

                        @foreach($ads as $key=>$value)
                            @php
                                $id = $value->id;
                            @endphp

                            <tr class="row1" data-id="{{ $id }}">
                                <td>{{ ++$key }}</td>
                                <td>{{ $value->title }}</td>
                               {{-- <td>{{ $value->no_of_image }}</td>--}}
                                <td>{{ $value->image_size }}</td>
                                <td>{{ $value->price }}</td>
                                <td class="text-center">
                                    <a href="{{url(config('obiroto.adminRoute').'/ads_types/'.$value->id.'/edit')}}"
                                       class="btn btn-warning btn-xs" data-toggle="tooltip" title="Edit"
                                       style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>

                                    {!! Form::open(['action' => ['Admin\Ads_TypesController@destroy',$value->id],'method' => 'delete','style'=>'display:inline']) !!}
                                    <button class="btn btn-danger btn-xs text-white" data-toggle="tooltip" title="Delete"
                                            style="display:inline;padding:2px 5px 3px 5px;" onclick="return confirm('Are you sure to delete this?')"><i class="fas fa-times"></i>
                                    </button>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

@endsection