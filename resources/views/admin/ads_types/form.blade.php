@extends('layouts.master')

@section('css')
    <!-- Plugin css -->
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            @if(isset($item))
                {!! Form::model($item, ['route' => ["ads_types.update", $item->id],'method' =>'PUT', 'class' => 'custom-validation', 'files' => true,'role'=>"form", 'id' => 'edit-form']) !!}
            @else
                {!! Form::open(['route' => ["ads_types.store"], 'method'=>'POST', 'class' => 'custom-validation', 'files' => true, 'role'=>"form",'id' => 'add-form']) !!}
            @endif
            <form class="custom-validation" action="#">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('title', trans('messages.title')) !!} <span class="la-required">*</span>
                            {!! Form::text('title', null,['class'=>'form-control', 'placeholder'=>trans('messages.enter_title'), 'required'=>'required']) !!}
                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('image_size', 'Image Size') !!} <span class="la-required">*</span>
                            {!! Form::text('image_size', null,['class'=>'form-control', 'placeholder'=>'Image Size', 'required'=>'required']) !!}
                            {!! $errors->first('image_size', '<p class="help-block">:message</p>') !!}
                            <span style="color: green">Example : 250 * 200</span>
                        </div>
                    </div>
                  {{--  <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('no_of_image', 'No Of Image') !!} <span class="la-required">*</span>
                            {!! Form::text('no_of_image', null,['class'=>'form-control', 'placeholder'=>'No of Image', 'required'=>'required']) !!}
                            {!! $errors->first('no_of_image', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>--}}
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('price', 'Price') !!}
                            {!! Form::text('price', null,['class'=>'form-control', 'placeholder'=>'Enter Price']) !!}
                            {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group mb-0">
                            <div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                    Submit
                                </button>
                                <a href={{route('categories.index')}}>
                                    <button type="button" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div> <!-- end col -->

@endsection

@section('script')
    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
//multiple select
            $('#category').select2({
                placeholder: "Select Category"
            });
        });
    </script>
@endsection