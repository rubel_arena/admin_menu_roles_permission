@extends('layouts.master')

@section('css')
    <!-- Plugin css -->
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            @if(isset($item))
                {!! Form::model($item, ['route' => ["ads.update", $item->id],'method' =>'PUT', 'class' => 'custom-validation', 'files' => true,'role'=>"form", 'id' => 'edit-form']) !!}
            @else
                {!! Form::open(['route' => ["ads.store"], 'method'=>'POST', 'class' => 'custom-validation', 'files' => true, 'role'=>"form",'id' => 'add-form']) !!}
            @endif
            <form class="custom-validation" action="#">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('ads_type_id', 'Ads Types', ['class' => 'control-label']) !!}<span
                                    class="la-required">*</span>
                            {!! Form::select('ads_type_id', $adsTypes, old('ads_type_id'), ['class' => 'form-control select2', 'id' => 'categories', 'required']) !!}
                            {!! $errors->first('ads_type_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('link', trans('messages.Link')) !!} <span class="la-required">*</span>
                            {!! Form::url('link', null,['class'=>'form-control', 'placeholder'=>'Enter Link', 'required'=>'required']) !!}
                            {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    @if(!isset($item))
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('images', trans('messages.images')) !!} <span
                                        class="la-required">*</span>
                                <br>
                                <label class="btn btn-success btn-cons m-b-10">
                                    <i class="fas fa-upload"></i>&nbsp <span>Upload</span>
                                    {!! Form::file('image',['style'=>"display: none;",'id'=>"image_upload"]) !!}
                                </label><br>
                                {!! $errors->first('images', '<p class="help-block">:message</p>') !!}
                                <span class="suggestion_text" id="image-size-suggestion"></span>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="update-image-wrapper">
                        <div class="col-lg-4">
                            @if(isset($item) && isset($item->image))
                                <div class="col-lg-4">
                                    <img width="100px" height="100px" src="{{asset($item->image)}}"
                                         alt="{{$item->adType->title}}">
                                </div>
                                <div class="col-lg-8">
                                    <label class="btn btn-success btn-cons m-b-10">
                                        <i class="fas fa-upload"></i>&nbsp <span>Upload</span>
                                        {!! Form::file('image',['style'=>"display: none;",'class'=>"image_upload"]) !!}
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row row-centered">
                    <div class="col-lg-10 col-centered">
                        <div id="image-preview">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group mb-0">
                            <div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                    Submit
                                </button>
                                <a href={{route('ads.index')}}>
                                    <button type="button" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div> <!-- end col -->

@endsection

@section('script')
    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

    <script type="text/javascript">
        function getImageSize(id) {
            $('#image-size-suggestion').hide();
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "{{ url(config('obiroto.adminRoute').'/get-ads-size') }}",
                data: {
                    id: id,
                    _token: '{{csrf_token()}}'
                },
                beforeSend: function () {
                    $("#loader").show();
                },
                success: function (data) {
                    $("#loader").hide();
                    $('#image-size-suggestion').empty();
                    if (data.schema == 0) {
                        $.alert('No Donors Yet')
                    } else {
                        $('#image-size-suggestion').show();
                        $('#image-size-suggestion').html(`<i class="fa fa-hand-o-right"
                                       aria-hidden="true"></i> Type: jpg,jpeg,png,bmp,tiff. Size: ${data.data} px`);
                    }
                }
            });
        }
        $(function () {
//multiple select
            $('#category').select2({
                placeholder: "Select Category"
            });
            $('#image_upload').on('change', function () {
                imagesPreview(this, 'div#image-preview', false);
            });

        });
    </script>
@endsection