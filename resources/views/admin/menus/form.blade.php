@extends('layouts.master')

@section('css')
    <!-- Plugin css -->
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            @if(isset($item))
                {!! Form::model($item, ['route' => ["menues.update", $item->id],'method' =>'PUT', 'class' => 'custom-validation', 'files' => true,'role'=>"form", 'id' => 'edit-form']) !!}
            @else
                {!! Form::open(['route' => ["menues.store"], 'method'=>'POST', 'class' => 'custom-validation', 'files' => true, 'role'=>"form",'id' => 'add-form']) !!}
            @endif
            <form class="custom-validation" action="#">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('parent_id', 'Categories') !!}
                            {!! Form::select('parent_id', $menueItems, isset($parent_ids) ? $parent_ids : null, ['class' => 'form-control select2', 'id' => 'category']) !!}
                            {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('menu_name', "Menu Name") !!} <span class="la-required">*</span>
                            {!! Form::text('menu_name', null,['class'=>'form-control', 'placeholder'=>'Enter Menu Name', 'required'=>'required']) !!}
                            {!! $errors->first('menu_name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
              {{--  <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <select class="form-control" name="icon_id">
                                @foreach($icons as $icon)
                                    <option value="{{$icon->id}}">
                                        <a href=""> <i class="{{$icon->icon_class}}"></i></a>
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>--}}
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group mb-0">
                            <div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                    Submit
                                </button>
                                <a href={{route('menues.index')}}>
                                    <button type="button" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div> <!-- end col -->

@endsection

@section('script')
    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
//multiple select
            $('#category').select2({
                placeholder: "Select Menu"
            });
        });
    </script>
@endsection