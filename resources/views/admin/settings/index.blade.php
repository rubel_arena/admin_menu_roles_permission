@extends('layouts.master')

@section('css')
    <!-- datatables css -->
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                {!! Form::model($item, ['route' => ["settings.update", $item->id],'method' =>'PUT', 'class' => 'custom-validation', 'files' => true,'role'=>"form", 'id' => 'edit-form']) !!}

                <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#organization-info" role="tab">
                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                <span class="d-none d-sm-block">Organization Info</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#social-link" role="tab">
                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                <span class="d-none d-sm-block">Social Link</span>
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active p-3" id="organization-info" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('brand_name', trans('messages.Organization Name')) !!} <span
                                                class="la-required">*</span>
                                        {!! Form::text('brand_name', null,['class'=>'form-control', 'placeholder'=>'Organization Name', 'required'=>'required']) !!}
                                        {!! $errors->first('brand_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('entrepreneurship_video_link', 'Entrepreneurship Youtube Link') !!}
                                        <span class="la-required">*</span>
                                        {!! Form::url('entrepreneurship_video_link', null,['class'=>'form-control', 'placeholder'=>'Entrepreneurship Youtube Link', 'required'=>'required']) !!}
                                        {!! $errors->first('entrepreneurship_video_link', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="logo">@lang('messages.Site Logo')</label>
                                        <div style="margin-top: 0" class="media">
                                            <div class="media-left">
                                                @if(!is_null($item->logo))
                                                    <img src="{{asset($item->logo)}}" alt="About Image"
                                                         width="80px"
                                                         height="40px">
                                                @else
                                                    <img src="{{asset('uploads/default/profile_image.png')}}"
                                                         alt="No Image"
                                                         width="80px" height="40px">
                                                @endif
                                            </div>
                                            <div class="media-body">
                                                <input type="file" id="logo" class="form-control" name="logo"
                                                       accept="image/png,image/gif,image/jpeg,image/jpg">
                                                <span class="suggestion_text"><i class="fa fa-hand-o-right"
                                                                                 aria-hidden="true"></i> Size:134X47px
                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="favicon">@lang('messages.Site Favicon')</label>

                                        <div style="margin-top: 0" class="media">
                                            <div class="media-left">
                                                @if(!is_null($item->favicon))
                                                    <img src="{{asset($item->favicon)}}" alt="About Image"
                                                         width="80px" height="40px">
                                                @else
                                                    <img src="{{asset('uploads/default/profile_image.png')}}"
                                                         alt="No Image"
                                                         width="80px" height="40px">
                                                @endif
                                            </div>
                                            <div class="media-body">
                                                <input type="file" id="favicon" class="form-control" name="favicon"
                                                       accept="image/png,image/gif,image/jpeg,image/jpg">
                                                <span class="suggestion_text"><i class="fa fa-hand-o-right"
                                                                                 aria-hidden="true"></i> Size:16X16px
                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('email', trans('messages.Email')) !!} <span
                                                class="la-required">*</span>
                                        {!! Form::text('email', null,['class'=>'form-control', 'placeholder'=>'Email', 'required'=>'required']) !!}
                                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('phone', 'Phone') !!}
                                        <span class="la-required">*</span>
                                        {!! Form::text('phone', null,['class'=>'form-control', 'placeholder'=>'Phone', 'required'=>'required']) !!}
                                        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        {!! Form::label('address', trans('messages.Address')) !!} <span
                                                class="la-required">*</span>
                                        {!! Form::text('address', null,['class'=>'form-control', 'placeholder'=>'Address', 'required'=>'required']) !!}
                                        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane p-3" id="social-link" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('fb_link', trans('messages.Facebook Link')) !!}
                                        {!! Form::url('fb_link', null,['class'=>'form-control', 'placeholder'=>'Facebook Link']) !!}
                                        {!! $errors->first('fb_link', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('twitter_link', trans('messages.Twitter Link')) !!}
                                        {!! Form::text('twitter_link', null,['class'=>'form-control', 'placeholder'=>'Twitter Link']) !!}
                                        {!! $errors->first('twitter_link', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('pinterest_link', trans('messages.Pinterest Link')) !!}
                                        {!! Form::url('pinterest_link', null,['class'=>'form-control', 'placeholder'=>'Pinterest Link']) !!}
                                        {!! $errors->first('pinterest_link', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('youtube_link', trans('messages.Youtube Link')) !!}
                                        {!! Form::text('youtube_link', null,['class'=>'form-control', 'placeholder'=>'Twitter Link']) !!}
                                        {!! $errors->first('youtube_link', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-0">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')


@endsection