@extends('layouts.master')

@section('css')
    <!-- Plugin css -->
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>

    <style>
        .regular-checkbox {
            -webkit-appearance: none;
            background-color: #fafafa;
            border: 1px solid #cacece;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
            padding: 9px !important;
            border-radius: 3px;
            display: inline-block;
            position: relative;
            margin-right: 6px;
        }

        .regular-checkbox:active, .regular-checkbox:checked:active {
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px 1px 3px rgba(0, 0, 0, 0.1);
        }

        .regular-checkbox:checked {
            background-color: #e9ecee;
            border: 1px solid #adb8c0;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
            color: #99a1a7;
        }

        .regular-checkbox:checked:after {
            content: '\2714';
            font-size: 26px;
            position: absolute;
            top: -13px;
            left: 2px;
            color: green;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Send Push Mail</h4>
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item active">Select Your Category and News</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">

            {!! Form::open(['route' => ["news_letter.store"], 'method'=>'POST', 'class' => 'custom-validation','role'=>"form",'id' => 'add-form']) !!}
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::label('category_id', 'Categories') !!}
                        <span class="la-required">*</span>
                        {!! Form::select('category_id[]', $categories, isset($category_ids) ? $category_ids : null, ['class' => 'form-control select2 select2-multiple', 'id' => 'category', 'required'=>'required','multiple' => 'multiple']) !!}
                        {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::label('subject', 'Mail Subject') !!}
                        {!! Form::text('subject', null,['class'=>'form-control', 'placeholder'=>'Enter Subject']) !!}
                        {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>

            <div class="wrapper"></div>
            {!! Form::close()!!}
        </div>
    </div> <!-- end col -->

@endsection

@section('script')
    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

    <script type="text/javascript">
        $(function () {
//multiple select
            $('#category').select2({
                placeholder: "Select Category"
            });
            $('#image_upload').on('change', function () {
                imagesPreview(this, 'div#image-preview', false);
            });

            $('#category').on('change', function (e) {
                $('.wrapper').hide();
                var category_ids = $(this).val();
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url(config('obiroto.adminRoute').'/news-letter/load') }}",
                    data: {
                        category_ids: category_ids,
                        _token: '{{csrf_token()}}'
                    },
                    beforeSend: function () {
                        $("#loader").show();
                    },
                    success: function (data) {
                        $("#loader").hide();
                        if (data.schema == 0) {
                            $.alert('No Donors Yet')
                        } else {
                            $('.wrapper').show();
                            $('.wrapper').html(data.schema);
                        }
                    }
                });
            });

        });
    </script>
@endsection