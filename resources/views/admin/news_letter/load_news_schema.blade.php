<div class="row" id="news_list">
    @foreach($categories as $category)
        <div class="col-md-12">
            <h4 class="my-3 lead category-title">{{$category->title}}</h4>
        </div>
        @forelse($category->news as $news)
            <div class="col-md-6">
                <div class="media">
                    <input type="checkbox" id="fruit-{{$news->id}}" name="news[]" class="regular-checkbox" value="{{$news->id}}">
                    <div class="media-body">
                        <p class="mt-0">{{$news->title}}</p>
                    </div>
                </div>
            </div>
        @empty
           <div class="col-md-6">
               <p class="empty-content">Empty Content</p>
           </div>
        @endforelse
    @endforeach
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            {!! Form::label('note', 'Short Note') !!} <span
                    class="la-required">*</span>
            {!! Form::textarea('note', null,['class'=>'form-control', 'placeholder'=>"Enter something", 'required'=>'required', 'rows' => 3, 'columns' => 3]) !!}
            {!! $errors->first('note', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group mb-0">
            <div>
                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i>
                    Send
                </button>
            </div>
        </div>
    </div>
</div>