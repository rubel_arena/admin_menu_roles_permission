@extends('layouts.master')

@section('css')
    <!-- datatables css -->
    <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                           style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr class="success">
                            <th>@lang('messages.Serial No.')</th>
                            <th>@lang('messages.Title')</th>
                            <th>@lang('messages.Designation')</th>
                            <th>@lang('messages.Logo')</th>
                            <th>@lang('messages.Status')</th>
                            <th class="text-center" width="200px">@lang('messages.Actions')</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($jobs as $key=>$value)
                            @php
                                $id = $value->id;
                            @endphp

                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $value->title }}</td>
                                <td>{{ $value->designation}}</td>
                                <td>
                                    <img src="{{asset($value->logo)}}" alt="{{$value->title}}" width="50px" height="50px">
                                </td>
                                <td class="text-center">
                                    @if($value->is_published == 1)
                                        <span class="badge badge-success">Published</span>
                                    @else
                                        <span class="badge badge-warning">Not Published</span>
                                    @endif

                                </td>
                                <td class="text-center">
                                    <a href="{{url(config('obiroto.adminRoute').'/latest_jobs/'.$value->id.'/edit')}}"
                                       class="btn btn-warning btn-xs" data-toggle="tooltip" title="Edit"
                                       style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>

                                    {!! Form::open(['action' => ['Admin\Latest_JobsController@destroy',$value->id],'method' => 'delete','style'=>'display:inline']) !!}
                                    <button class="btn btn-danger btn-xs text-white" data-toggle="tooltip" title="Delete"
                                            style="display:inline;padding:2px 5px 3px 5px;" onclick="return confirm('Are you sure to delete this?')"><i class="fas fa-times"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

@endsection

@section('script')

    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

@endsection