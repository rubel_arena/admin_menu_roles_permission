@extends('layouts.master')

@section('css')
    <!-- Plugin css -->
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            @if(isset($item))
                {!! Form::model($item, ['route' => ["latest_jobs.update", $item->id],'method' =>'PUT', 'class' => 'custom-validation', 'files' => true,'role'=>"form", 'id' => 'edit-form']) !!}
            @else
                {!! Form::open(['route' => ["latest_jobs.store"], 'method'=>'POST', 'class' => 'custom-validation', 'files' => true, 'role'=>"form",'id' => 'add-form']) !!}
            @endif
            <form class="custom-validation" action="#">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('title', trans('messages.title')) !!} <span class="la-required">*</span>
                            {!! Form::text('title', null,['class'=>'form-control', 'placeholder'=>trans('messages.enter_title'), 'required'=>'required']) !!}
                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('designation', trans('messages.Designation')) !!} <span class="la-required">*</span>
                            {!! Form::text('designation', null,['class'=>'form-control', 'placeholder'=>trans('messages.enter_designation'), 'required'=>'required']) !!}
                            {!! $errors->first('designation', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            {!! Form::label('description', trans('messages.description')) !!} <span
                                    class="la-required">*</span>
                            {!! Form::textarea('description', null,['class'=>'form-control editors', 'placeholder'=>trans('messages.enter_description'), 'required'=>'required']) !!}
                            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('is_published', trans('messages.Status')) !!} <span
                                    class="la-required">*</span>
                            {!! Form::select('is_published', $published_status, isset($item->is_published) ? $item->is_published : 2, ['class' => 'form-control', 'required'=>'required']) !!}
                            {!! $errors->first('is_published', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    @if(!isset($item))
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('images', trans('messages.images')) !!} <span
                                        class="la-required">*</span>
                                <br>
                                <label class="btn btn-success btn-cons m-b-10">
                                    <i class="fas fa-upload"></i>&nbsp <span>Upload</span>
                                    {!! Form::file('logo',['style'=>"display: none;",'id'=>"image_upload"]) !!}
                                </label><br>
                                {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
                                <span class="suggestion_text"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Type: jpg,jpeg,png,bmp,tiff. Size: 560X353 px</span>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="row">
                    <div class="update-image-wrapper">
                        <div class="col-lg-4">
                            @if(isset($item) && isset($item->logo))
                                <div class="col-lg-4">
                                    <img width="100px" height="100px" src="{{asset($item->logo)}}"
                                         alt="{{$item->title}}">
                                </div>
                                <div class="col-lg-8">
                                    <label class="btn btn-success btn-cons m-b-10">
                                        <i class="fas fa-upload"></i>&nbsp <span>Upload</span>
                                        {!! Form::file('logo',['style'=>"display: none;",'class'=>"image_upload"]) !!}
                                    </label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row row-centered">
                    <div class="col-lg-10 col-centered">
                        <div id="image-preview">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group mb-0">
                            <div>
                                <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                    Submit
                                </button>
                                <a href={{route('latest_jobs.index')}}>
                                    <button type="button" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div> <!-- end col -->

@endsection

@section('script')
    <!-- Plugins js -->
    <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

    <script type="text/javascript">
        /*Multiple image brows after show*/
        $(function () {
            $('#image_upload').on('change', function () {
                imagesPreview(this, 'div#image-preview', false);
            });

        });
    </script>
@endsection