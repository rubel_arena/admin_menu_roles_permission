Dear Honorable User,<br><br>

You have been succesfully subscribed on Obiroto. Now, you have to verify your email.<br><br>

<h1>
	<a href="{{ config('obiroto.frontEndBaseUrl')."subscriber_verification/$user->verify_token" }}">Click here to verify your email</a>
</h1>
<br><br>

Best Regards,<br>
Obiroto