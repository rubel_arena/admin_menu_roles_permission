@php
    $title = '';
    $action = '';

    $route = Route::currentRouteName();
    if ($route) {
     $routeArray = explode('.', $route);
    list($title, $action) = $routeArray;
    }

@endphp
<!-- start page title -->
@if($route)
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">
                    <a href={{url('/')}}>{{ucfirst(config('obiroto.adminRoute'))}}</a>
                </h4>
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href={{route($title.'.'.'index')}}>{{ucfirst($title)}}</a></li>
                    <li class="breadcrumb-item active"><a>{{ucfirst($action)}}</a></li>
                </ol>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="float-right d-none d-md-block">
                @if($action === 'index')
                    <a href={{route($title.'.'.'create')}}>
                        <button class="btn btn-primary waves-effect waves-light" type="button">
                            <i class="ti-plus mr-2"></i> {{config('obiroto.Add')}}
                        </button>
                    </a>
                @else
                    <a href={{route($title.'.'.'index')}}>
                        <button class="btn btn-success waves-effect waves-light" type="button">
                            <i class="fas fa-arrow-alt-circle-left"></i> &nbsp;{{config('obiroto.Back')}}
                        </button>
                    </a>
                @endif
            </div>
        </div>
    </div>
@endif
<!-- end page title -->