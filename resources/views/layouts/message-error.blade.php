@if(session()->has('message'))
 <div class="alert alert-success alert-dismissable">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>{{ session()->get('message') }}</strong>
 </div>
@endif
{{--{{dd($errors)}}--}}
@if (count($errors) > 0)
 <div class="alert alert-danger">
  <ul>
   @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
   @endforeach
  </ul>
 </div>
@endif