<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">
    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Main</li>
                @foreach($menues as $menu)
                    @if($menu->has_child == 0)
                        <li>
                            <a href={{route($menu->slug.'.index')}} class="waves-effect">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <span>{{$menu->menu_name}}</span>
                            </a>
                        </li>
                    @else
                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">
                                <i class="far fa-newspaper"></i>
                                <span>{{$menu->menu_name}}</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                @foreach($menu->subMenues as $subMenue)
                                    <li><a href={{route($subMenue->slug.'.index')}}>{{$subMenue->menu_name}}</a></li>
                                @endforeach
                            </ul>
                        </li>

                    @endif
                @endforeach

                {{--<li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-photo-video"></i>
                        <span>Manage Media</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href={{route('gallery.index')}}>Gallery</a></li>
                    </ul>
                </li>


                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-ad"></i>
                        <span>Manage Ads</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href={{route('ads_types.index')}}>Ads Types</a></li>
                        <li><a href={{route('ads.index')}}>Ads</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-user-tie"></i>
                        <span>Manage Jobs</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href={{route('latest_jobs.index')}}>Jobs</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fa fa-users"></i>
                        <span>Manage Newsletter</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href={{url(config('obiroto.adminRoute') . '/manage/news-letter')}}>Push Mail</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="fas fa-tools"></i>
                        <span>Web Setting</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href={{route('settings.index')}}>Site Setting</a></li>
                    </ul>
                </li>--}}
            </ul>


            {{--  <li>
                  <a href="javascript: void(0);" class="has-arrow waves-effect">
                      <i class="fab fa-black-tie"></i>
                      <span>Manage Interviews</span>
                  </a>
                  <ul class="sub-menu" aria-expanded="false">
                      <li><a href={{route('interviews.index')}}>Interview</a></li>
                  </ul>
              </li>--}}
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->