<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <p class="mb-0">© <script>document.write(new Date().getFullYear())</script> {{$setting->brand_name}}. Crafted with <i class="mdi mdi-heart text-danger"></i> by
                    <a target="_blank" href="https://www.arena.com.bd/">Arena Phone BD LTD</a></p>            </div>
        </div>
    </div>
</footer>